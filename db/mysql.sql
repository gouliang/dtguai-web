-- 菜单
CREATE TABLE `sys_def_menu` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单主键',
  `parent_id` bigint(22) COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) COMMENT '菜单名称',
  `url` varchar(200) COMMENT '菜单URL',
  `perms` varchar(2000) COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` tinyint(4) COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) COMMENT '菜单图标',
  `orders` int COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- 系统用户
CREATE TABLE `sys_def_admin` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统用户主键',
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) COMMENT '密码',
  `code` varchar(20) COMMENT '随即串',
  `sex` tinyint(4) DEFAULT '1' COMMENT '性别0女1男',
  `email` varchar(100) COMMENT '邮箱',
  `mobile` varchar(100) COMMENT '手机号',
  `status` tinyint(4) COMMENT '状态  0：禁用   1：正常',
  `create_admin_id` bigint(20) COMMENT '创建者ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `remake` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- 角色
CREATE TABLE `sys_def_role` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色主键',
  `role_name` varchar(100) COMMENT '角色名称',
  `remark` varchar(100) COMMENT '备注',
  `create_admin_id` bigint(22) COMMENT '创建者ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_admin_id` bigint(22) COMMENT '更新人ID',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';

-- 用户与角色对应关系
CREATE TABLE `sys_def_admin_role` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户与角色对应关系主键',
  `sys_def_admin_id` bigint(22) COMMENT '用户ID',
  `sys_def_role_id` bigint(22) COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- 角色与菜单对应关系
CREATE TABLE `sys_def_role_menu` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色与菜单对应关系主键',
  `sys_def_role_id` bigint(22) COMMENT '角色ID',
  `sys_def_menu_id` bigint(22) COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- 系统配置信息
CREATE TABLE `sys_def_config` (
	`id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统配置信息主键',
	`param_key` varchar(50) COMMENT 'key',
	`param_value` varchar(2000) COMMENT 'value',
	`status` tinyint(4) DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
	`remark` varchar(500) COMMENT '备注',
	PRIMARY KEY (`id`),
	UNIQUE INDEX (`param_key`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='系统配置信息表';


-- 系统日志
CREATE TABLE `sys_def_log` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统日志主键',
  `user_name` varchar(50) COMMENT '用户名',
  `operation` varchar(50) COMMENT '用户操作',
  `method` varchar(200) COMMENT '请求方法',
  `params` varchar(5000) COMMENT '请求参数',
  `time` bigint NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) COMMENT 'IP地址',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `old` varchar(5000) COMMENT '旧信息',
  PRIMARY KEY (`id`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='系统日志';


-- 文件上传
CREATE TABLE `sys_def_oss` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件上传主键',
  `url` varchar(500) COMMENT 'URL地址',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `document_type` tinyint(3) DEFAULT NULL COMMENT '文档类型',
  `type` tinyint(3) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='文件上传';


-- 文件上传 fdfs
CREATE TABLE `sys_def_fdfs` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `size` varchar(50) DEFAULT NULL COMMENT '大小',
  `group_name` varchar(50) NOT NULL COMMENT '组名',
  `fast_file_id` varchar(255) NOT NULL COMMENT 'fdfs_id',
  `type` tinyint(2) DEFAULT NULL COMMENT '状态',
  `document_type` tinyint(2) DEFAULT NULL COMMENT '文档类型',
  `create_id` bigint(22) DEFAULT NULL COMMENT '创建人id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `url` varchar(255) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COMMENT='fdfs'







-- 初始数据 
INSERT INTO `sys_def_admin` (`id`, `name`, `password`, `code`, `email`, `mobile`, `status`, `create_admin_id`) VALUES ('1', 'admin', 'd789cb2415affd3698ff9c3ccbf0aefe6ef57ae85506f7f2cb0a5f2f98e34843', 'TKSMjNPaehqLtwBXwG4z', '123086461@qq.com', '18600231871', '1', '1');

INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);

INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (5, 11, 'SQL监控', 'http://localhost:9094/druid/sql.html', NULL, 1, 'sql', 4);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (6, 11, '程序监控', 'http://127.0.0.1:9094/monitoring', NULL, 1, 'sql', 4);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (7, 11, '接口文档', 'http://localhost:9094/doc.html', NULL, 1, 'sql', 4);

INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (8, 12, '代码生成', 'http://generator.dtguai.com/', NULL, 1, 'sql', 4);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (9, 12, '表单设计', 'https://form.avuejs.com/', NULL, 1, 'sql', 4);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (10, 12, '视图设计', 'https://crud.avuejs.com/', NULL, 1, 'sql', 4);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (13, 12, '数据大屏', 'https://data.avuejs.com/', NULL, 1, 'sql', 4);

INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (11, 0, '监控/文档', NULL, NULL, 0, 'system', 1);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (12, 0, '开发工具', NULL, NULL, 0, 'system', 1);

INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 8);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (30, 1, '文件上传', 'oss/oss', 'sys:oss:all', 1, 'oss', 6);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (32, 1, 'fdfs', 'fdfs/fdfs', 'sys:fdfs:all', 1, 'oss', 7);
INSERT INTO `sys_def_menu`(`id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `orders`) VALUES (31, 0, '业务管理', NULL, NULL, 0, 'system', 1);


INSERT INTO `sys_def_config` (`param_key`, `param_value`, `status`, `remark`) VALUES ('SYS:DEF:CONFIG:CLOUD:STORAGE:KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');
