#!/bin/sh

nohup java -jar dtguai-web-1.0.0-SNAPSHOT.jar -Djavamelody.x-frame-options=ALLOW-FROM >output 2>output & #注意：必须有&让其后台执行，否则没有pid生成
echo $! > apppid.pid # 将jar包启动对应的pid写入文件中，为停止时提供pid