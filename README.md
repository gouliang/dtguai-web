### 项目说明
<a href="https://cn.vuejs.org/index.html">
            <img src="https://img.shields.io/badge/vue-2.5.17-blue.svg" alt="vue">
        </a>    
        +
        <a href="http://spring.io/projects/spring-boot">
            <img src="https://img.shields.io/badge/spring--boot-2.4.4-green.svg" alt="spring-boot">
        </a>
        <a href="https://shiro.apache.org/">
            <img src="https://img.shields.io/badge/shiro-1.4.0-blue.svg" alt="shiro">
</a>     

   
----------------------------------------最后更新时间2019年9月19日15:22:26-----------------------------     

启动配置需要添加 才能看到 监控
-Djavamelody.x-frame-options=ALLOW-FROM



dtguai-web是一个轻量级的，前后端分离的Java快速开发平台  
目前支持:MySQL dm7 oracle    
前端地址：https://gitee.com/gouliang/dtguai-vue   
代码生成器源码: https://gitee.com/gouliang/dtguai-code_generator   
app-api地址:https://gitee.com/gouliang/dtguai-app-api  

演示地址： http://web.dtguai.com/static/index.html (账号密码：admin/admin)     
后台接口地址:http://web.dtguai.com/   
代码生成:http://generator.dtguai.com    

### 更新日志  
https://gitee.com/gouliang/dtguai-web/blob/master/CHANGELOG   

### 开发必要环境:
1.jdk1.8及期以上  
2.lombok插件(请自行根据ide安装)   
3.ide工具(myeclipse idea sts等随意) 

### 超级无敌傻瓜使用教程  
### 5分钟上手
1.clone 项目 引入 ide  
2.启动项目  
### 5分钟使用  
1.http://generator.dtguai.com 找对应的数据库  
2.选择需要生成的表   
3.点击生成按钮   
4.覆盖到项目  

### 后端部署

通过git下载源码
创建数据库，数据库编码为UTF-8   
执行db/mysql.sql文件，初始化数据   
修改application-dev.yml，更新MySQL账号和密码    
Eclipse、IDEA运行Application.java，则可启动项目   
Swagger路径：http://localhost:9094/  

### 前端部署

本项目是前后端分离的，还需要部署前端，才能运行起来  
前端下载地址：https://gitee.com/gouliang/dtguai-vue  
前端部署完毕，就可以访问项目了，账号：admin，密码：admin  


###  **---------------------以下都是废话看不看均可-----------------------------------------------------** 



### 具有如下特点
实现前后端分离，通过token进行数据交互  
灵活的权限控制，可控制到页面或按钮  
页面交互使用Vue2
完善的代码生成机制，可在线生成model、xml、dao、service、vue、sql代码，减少70%以上的开发任务   
引入Hibernate Validator校验框架，轻松实现后端校验   
引入云存储服务，已支持：FastDFS、阿里云、HDFS等   
引入swagger文档支持，方便编写API接口文档,和后续自动化测试   



### 项目结构



dtguai-web    
├─db  项目SQL语句  
├─doc 文档  
├─log 日志文件   
│  
├─common 公共模块  
│  ├─aspect 系统日志  
│  ├─exception 异常处理  
│  ├─validator 后台校验  
│  └─xss XSS过滤  
│   
├─config 配置信息  
│ 
├─web 功能模块   
│  ├─modules 业务模块(生成代码存放目录)    
│  ├─oss 文件服务模块   
│  └─sys 权限模块   
│ 
├─Application 项目启动类  
│    
├──resources   
│  ├─mapper SQL对应的XML文件  
│  └─static 静态资源  
  
 
  
 
### 技术选型：

核心框架：Spring Boot 2.1.0.RELEASE  
安全框架：Apache Shiro 1.4  
视图框架：Spring MVC 5.0  
持久层框架：MyBatis 3.4  
数据库连接池：Druid 1.1.10   
日志管理：SLF4J 1.7、Log4j  1.2.17     
页面交互：Vue2.x  

### 基本功能
# 功能截图
![输入图片说明](https://gitee.com/gouliang/dtguai-web/raw/master/doc/1.png)

![输入图片说明](https://gitee.com/gouliang/dtguai-web/raw/master/doc/2.png )

## License
Apache License Version 2.0

