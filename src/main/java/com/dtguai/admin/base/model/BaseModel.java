package com.dtguai.admin.base.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Transient;

/**
 * @author: guo
 * @Date: 2019/1/16 19:13
 * @Description:
 */
public class BaseModel {
    @ApiModelProperty(hidden=true)
    @Transient
    private Integer pageNum;

    @ApiModelProperty(hidden=true)
    @Transient
    private Integer pageSize;

    @ApiModelProperty(hidden=true)
    @Transient
    private String orderBy;

    /**
     * 是否分页
     */
    @ApiModelProperty(hidden=true)
    @Transient
    private Boolean countSql;

    @ApiModelProperty(hidden=true)
    @Transient
    private Boolean pageSizeZero;

    @ApiModelProperty(hidden=true)
    @Transient
    private Boolean reasonable;

    /**
     * 排序列名
     */
    @ApiModelProperty(hidden=true)
    @Transient
    private String sortname;

    /**
     * 页排序方向
     */
    @ApiModelProperty(hidden=true)
    @Transient
    private String sortorder;

    /**
     * id分页 用来优化sql 命中索引提高效率 all<index<range
     */
    @ApiModelProperty(hidden=true)
    @Transient
    private Integer pageByid;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setOrderBy(String sortname, String sortorder) {
        if (null != sortname && null != sortorder) {
            this.orderBy = sortname + " " + sortorder;
        }
    }

    public Boolean getCountSql() {
        return countSql;
    }

    public void setCountSql(Boolean countSql) {
        this.countSql = countSql;
    }

    public Boolean getPageSizeZero() {
        return pageSizeZero;
    }

    public void setPageSizeZero(Boolean pageSizeZero) {
        this.pageSizeZero = pageSizeZero;
    }

    public Boolean getReasonable() {
        return reasonable;
    }

    public void setReasonable(Boolean reasonable) {
        this.reasonable = reasonable;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public String getSortorder() {
        return sortorder;
    }

    public void setSortorder(String sortorder) {
        this.sortorder = sortorder;
    }

    public Integer getPageByid() {
        return pageByid;
    }

    public void setPageByid(Integer pageByid) {
        this.pageByid = pageByid;
    }
}
