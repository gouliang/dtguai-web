package com.dtguai.admin.base.form;

import com.dtguai.admin.base.model.BaseModel;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * BaseFrom
 *
 * @author guo
 * @date 2019年1月15日18:23:48
 */
@Data
public class BaseFrom extends BaseModel {

    @ApiModelProperty(value = "主键")
    @NotNull(message = "id不能为空", groups = {UpdateGroup.class})
    private Integer id;
}
