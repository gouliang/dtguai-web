package com.dtguai.admin.base.mapper;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * BaseMapper
 *
 * @author guo
 * @date 2019年1月15日18:23:06
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {

}
