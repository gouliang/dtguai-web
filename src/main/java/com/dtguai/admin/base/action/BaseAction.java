package com.dtguai.admin.base.action;

import com.dtguai.admin.web.sys.model.SysDefAdmin;
import org.apache.shiro.SecurityUtils;

/**
 * 基础类
 *
 * @author guoLiang
 * @date 2019年10月10日11:36:44
 */
public abstract class BaseAction {

    public SysDefAdmin getAdmin() {
        return (SysDefAdmin) SecurityUtils.getSubject().getPrincipal();
    }

    public Integer getAdminId() {
        return getAdmin().getId();
    }
}
