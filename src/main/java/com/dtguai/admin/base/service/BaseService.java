package com.dtguai.admin.base.service;

import com.dtguai.admin.base.model.BaseModel;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;

/**
 * @author guoLiang
 * @date 2018-09-29 03:46:03
 */

@Service
public interface BaseService<T> {

    /**
     * 描述
     *
     * @param key key
     * @return T
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    T selectByKey(Object key);

    /**
     * 描述 保存一个实体，null的属性不会保存，会使用数据库默认值
     *
     * @param entity 实体
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int insertSelective(T entity);

    /**
     * 描述
     *
     * @param entity 实体
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int save(T entity);

    /**
     * 描述
     *
     * @param key key
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int delete(Object key);

    /**
     * 描述
     *
     * @param entity example对象
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int updateAll(T entity);

    /**
     * 描述
     *
     * @param entity example对象
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int updateNotNull(T entity);

    /**
     * 描述
     *
     * @param example example对象
     * @return List<T>
     * @author guoLiang
     * @date 2018-09-29 03:46:03
     */
    List<T> selectByExample(Object example);

    /**
     * 描述
     *
     * @param example example对象
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int selectCount(T example);

    /**
     * 根据实体属性作为条件进行删除，查询条件使用等号
     * 此操作杀伤力太强请谨慎使用
     *
     * @param example example
     * @return int
     * @author: guoLiang
     * @date 2018-09-29 03:46:03
     */
    int deleteByWhere(T example);

    /**
     * 批量删除
     *
     * @param ids 主键
     * @param c   class
     */
    void deleteByIds(List<Integer> ids, Class c);

    /**
     * 条件查询
     *
     * @param ec   查询条件
     * @param c    类型
     * @param page 是否分页
     * @author guoLiang
     * @date 2019年1月23日15:51:28
     */
    void getExample(Criteria ec, BaseModel c, boolean page);

    /**
     * 条件查询
     *
     * @param c    通用分页类型
     * @param page 是否分页
     * @param clas 需要转换的类
     * @return Example
     * @author guoLiang
     * @date 2019年1月23日15:52:21
     */
    Example getExample(BaseModel c, boolean page, Class<?> clas);

    /**
     * 条件查询
     *
     * @param bm   BaseMode类型
     * @param page 是否分页 true分页 false 不分页
     * @param c    要返回的对象class
     * @return List<T>
     * @author guo
     */
    List<T> selectByPage(BaseModel bm, boolean page, Class<T> c);

    /**
     * 条件查询
     *
     * @param bm   BaseMode类型
     * @param page 是否分页 true分页 false 不分页
     * @return List<T>
     * @author guo
     */
    List<T> selectByPage(BaseModel bm, boolean page);

    /**
     * 条件查询
     *
     * @param bm   BaseMode类型
     * @return List<T>
     * @author guo
     */
    List<T> selectByPage(BaseModel bm);


    /**
     * 根据实体中的属性进行查询，只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
     *
     * @param entity 实体
     * @return T
     */
    T selectOne(T entity);
}
