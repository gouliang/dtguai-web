package com.dtguai.admin.base.service.impl;

import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.base.model.BaseModel;
import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.common.xss.SqlFilter;
import com.dtguai.admin.page.PageHelperUtil;
import com.dtguai.admin.util.StringUtil;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

/**
 * @author guoLiang
 * @date 2018-09-29 03:46:03
 */
public abstract class BaseServiceImpl<T> extends BaseAction implements BaseService<T> {

    @Autowired
    protected Mapper<T> mapper;

    public Mapper<T> getMapper() {
        return mapper;
    }

    @Override
    public T selectByKey(Object key) {
        return Optional.ofNullable(key)
                .map(mapper::selectByPrimaryKey)
                .orElse(null);
    }

    @Override
    public int insertSelective(T entity) {
        //保存一个实体，null的属性不会保存，会使用数据库默认值
        return mapper.insertSelective(entity);
    }

    @Override
    public int save(T entity) {
        //保存一个实体，null的属性也会保存，不会使用数据库默认值
        return mapper.insert(entity);
    }

    @Override
    public int delete(Object key) {
        if (key == null) {
            return 0;
        }
        return mapper.deleteByPrimaryKey(key);
    }

    @Override
    public int updateAll(T entity) {
        return mapper.updateByPrimaryKey(entity);
    }

    @Override
    public int updateNotNull(T entity) {
        return mapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<T> selectByExample(Object example) {
        return mapper.selectByExample(example);
    }

    @Override
    public int selectCount(T example) {
        return mapper.selectCount(example);
    }

    /**
     * 根据实体属性作为条件进行删除，查询条件使用等号
     * 此操作杀伤力太大请谨慎使用
     *
     * @param example
     * @return
     */
    @Override
    public int deleteByWhere(T example) {
        return mapper.delete(example);
    }

    /**
     * 根据id批量删除 id in(1.2.3.4.5)
     *
     * @param ids
     */
    @Override
    public void deleteByIds(List<Integer> ids, Class c) {
        Example example = new Example(c);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", ids);
        mapper.deleteByExample(example);
    }

    @Override
    public Example getExample(BaseModel c, boolean page, Class<?> clas) {
        Example example = new Example(clas);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo(c);
        getExample(criteria, c, page);
        return example;
    }

    @Override
    public void getExample(Example.Criteria criteria, BaseModel c, boolean page) {
        if (StringUtil.isEmpty(c.getOrderBy())) {
            PageHelper.orderBy("id desc");
        } else {
            // 防止SQL注入（因为是通过拼接SQL实现排序的，会有SQL注入风险）
            PageHelper.orderBy(SqlFilter.sqlInject(c.getOrderBy()));
        }
        //判断是否分页
        if (page) {
            if (!StringUtil.isEmpty(c.getPageByid())) {
                String ASC = "asc";
                String DESC = "desc";
                if (PageHelper.getLocalPage().getOrderBy().toLowerCase().indexOf(DESC) > 0) {
                    criteria.andLessThan("id", c.getPageByid());
                } else if (PageHelper.getLocalPage().getOrderBy().toLowerCase().indexOf(ASC) > 0) {
                    criteria.andGreaterThan("id", c.getPageByid());
                }
            }
            PageHelperUtil.startPage(c);
        }
    }

    @Override
    public List<T> selectByPage(BaseModel bf, boolean page, Class<T> c) {
        return selectByExample(getExample(bf, page, c));
    }

    /**
     * 查询信息
     *
     * @param bm   实体
     * @param page 是否分页 true分页 false 不分页
     * @return List<T>
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> selectByPage(BaseModel bm, boolean page) {
        Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return selectByExample(getExample(bm, page, clazz));
    }

    /**
     * 查询信息
     *
     * @param bm 实体
     * @return List<T>
     */
    @Override
    public List<T> selectByPage(BaseModel bm) {
        return selectByPage(bm, true);
    }

    @Override
    public T selectOne(T entity) {
        return mapper.selectOne(entity);
    }
}