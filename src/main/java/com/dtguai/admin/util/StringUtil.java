package com.dtguai.admin.util;

/**
 * @author guoLiang
 * @date 2018-09-29 03:46:03
 */
public class StringUtil {

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str) || str.trim().length() == 0;
    }

    public static boolean isEmpty(Integer str) {
        return str == null;
    }

    public static String format(String str, String... args) {
        if (isEmpty(str)) {
            return str;
        }
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                str = str.replaceFirst("\\{\\}", args[i]);
            }
        }
        return str;
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0;
    }


    /**
     * 是否是包含"{}"的格式化字符串，例如：”错误原因{}“
     *
     * @param src
     * @return
     */
    public static boolean isFormatStr(String src) {
        String str = ".*\\{\\}.*";
        if (isEmpty(src)) {
            return false;
        }
        if (src.matches(str)) {
            return true;
        }
        return false;
    }
}
