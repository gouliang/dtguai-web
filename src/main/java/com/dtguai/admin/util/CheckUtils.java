package com.dtguai.admin.util;


import com.dtguai.admin.common.exception.DefinedException;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>辅助检测工具类</p>
 *
 * @author guo
 * @date 2019年6月25日09:31:46
 */
@Slf4j
public class CheckUtils {

    private CheckUtils() {
        throw new IllegalStateException("CheckUtils辅助检测工具类 不能实例化");
    }

    public static String checkAndGetKey(String k1, String k2, String keyName) {
        if (StringUtil.isNullOrEmpty(k1) && StringUtil.isNullOrEmpty(k2)) {
            log.error(String.format("%s is not configured (未配置%s)", keyName, keyName));
            throw new DefinedException(String.format("%s is not configured (未配置%s)", keyName, keyName));
        }
        if (k1 == null) {
            return k2;
        }
        return k1;
    }

}
