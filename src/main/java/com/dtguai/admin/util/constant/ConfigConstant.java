package com.dtguai.admin.util.constant;

/**
 * 系统参数相关Key
 *
 * @author guo
 * @date 2018年12月24日18:07:28
 */
public class ConfigConstant {
    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "SYS:DEF:CONFIG:CLOUD:STORAGE:KEY";
}
