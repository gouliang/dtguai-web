package com.dtguai.admin.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author guoLiang
 * @date 2019年1月23日14:51:01
 */
public class DateUtils {

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 字符戳转Date类型的字符串, 默认使用 yyyy-MM-dd hh:mm:ss 格式
     *
     * @param date date 时间
     */
    public static String format(Date date) {
        return format(date, DATE_TIME_FORMAT);
    }

    /**
     * 字符戳转Date类型的字符串, 默认使用 yyyy-MM-dd hh:mm:ss 格式
     *
     * @param date         date 时间
     * @param formatString String 字符串格式；如：yyyy-MM-dd hh:mm:ss
     */
    public static String format(Date date, String formatString) {
        if (formatString == null) {
            formatString = DATE_TIME_FORMAT;
        }
        DateFormat dd = new SimpleDateFormat(formatString);
        return dd.format(date);
    }

}
