package com.dtguai.admin.protocol.response;

import java.io.Serializable;

import com.dtguai.admin.common.log.OldLog;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.dtguai.admin.common.error.ErrorCode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author  guoLiang
 * @date 2019年7月9日11:23:23
 */
@Data
public class ApiResponse<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回代码")
    private int code = ErrorCode.NO_ERROR.getCode();

    @ApiModelProperty(value = "返回信息")
    private String msg = ErrorCode.NO_ERROR.getMessage();

    @ApiModelProperty(value = "返回时间")
    private String timestamp;

    @ApiModelProperty(value = "返回数据")
    @JsonInclude(Include.NON_NULL)
    private T result;

    @JsonIgnore
    @ApiModelProperty(value = "旧日志")
    private OldLog oldLog;


    public ApiResponse() {
        super();
    }

    public ApiResponse(T t) {
        this.result = t;
    }

    public ApiResponse(OldLog oldLog) {
        this.oldLog = oldLog;
    }

    public ApiResponse(T t, OldLog oldLog) {
        this.result = t;
        this.oldLog = oldLog;
    }

    public ApiResponse(ErrorCode protocolErrorCode) {
        this.code = protocolErrorCode.getCode();
        this.msg = protocolErrorCode.getMessage();
    }

    public ApiResponse(ErrorCode protocolErrorCode, String msg) {
        this.code = protocolErrorCode.getCode();
        this.msg = msg;
    }

}
