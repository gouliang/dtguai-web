
package com.dtguai.admin.api;

import com.dtguai.admin.common.annotation.SysLog;
import com.dtguai.admin.common.log.OldLog;
import com.dtguai.admin.common.log.OldLogImpl;
import com.dtguai.admin.protocol.response.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guo
 * @date 2020年3月24日09:55:48
 */
@RestController
@RequestMapping()
@Slf4j
public class Csrf {


    @GetMapping(value = "csrf", produces = "application/text;charset=UTF-8")
    public String csrf() {
        String csrf = "csrf---------------";
        log.info(csrf);
        return csrf;
    }

    @GetMapping(value = "favicon.ico", produces = "application/text;charset=UTF-8")
    public String favicon() {
        return "favicon---------------";
    }

}
