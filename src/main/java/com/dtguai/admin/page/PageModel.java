package com.dtguai.admin.page;

import com.dtguai.admin.base.model.BaseModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * @author guo
 * @date 2018-09-29 03:46:03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PageModel extends BaseModel {

    /**
     * id 主键
     * //@Column(name = "Id",insertable=false,updatable=false) //dm
     * //mysql
     */
    @Id
    @Column(name = "Id")
    @KeySql(useGeneratedKeys = true)
    @ApiModelProperty(value = "主键")
    private Integer id;

}

