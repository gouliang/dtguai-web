package com.dtguai.admin.page;

import com.dtguai.admin.base.model.BaseModel;
import com.github.pagehelper.PageHelper;

/**
 * @author guo
 * @date 2018-09-29 03:46:03
 */
public class PageHelperUtil {

    public static void startPage(BaseModel pm) {
        PageHelper.startPage(null == pm.getPageNum() ? 1 : pm.getPageNum(), null == pm.getPageSize() ? 10 : pm.getPageSize());
    }

}
