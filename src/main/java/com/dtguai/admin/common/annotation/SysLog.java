package com.dtguai.admin.common.annotation;

import java.lang.annotation.*;

/**
 * 系统日志注解
 *
 * @author guo
 * @date 2018年12月25日14:21:55
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value() default "";

}
