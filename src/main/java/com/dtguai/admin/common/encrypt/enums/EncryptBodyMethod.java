package com.dtguai.admin.common.encrypt.enums;

/**
 * <p>加密方式</p>
 *
 * @author guo
 * @date 2019年4月16日14:09:02
 */
public enum EncryptBodyMethod {
    /**
     * MD5
     */
    MD5,
    /**
     * DES
     */
    DES,
    /**
     * AES
     */
    AES,
    /**
     * SHA
     */
    SHA,
    /**
     * RSA
     */
    RSA

}
