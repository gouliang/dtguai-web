package com.dtguai.admin.common.error;

import java.util.HashMap;
import java.util.Map;

/**
 * 错误码定义类
 * <p>
 * 1000 - 9999 为系统错误码
 *
 * @author guo
 */
public enum ErrorCode {
    /**
     * 系统错误定义
     */
    NO_ERROR(200, "成功"),
    INTERNAL_SERVER_ERROR(500, "服务器错误,请联系管理员"),
    BAD_REQUEST(400, "参数有误"),
    SC_UNAUTHORIZED(401, "web-无效的token"),
    VERIFICATION_CODE_LOSE_EFFICACY(402, "web-验证码错误"),
    NOT_FOUND(404, "路径不存在，请检查路径是否正确"),
    REPETITION(405, "记录重复"),
    NOT_PERMISSION(406, "没有权限，请联系管理员授权"),
    NOT_PERMISSION_ADD_ROLE_NOT_CREATE(407, "新增用户所选角色，不是本人创建"),
    ADMIN_NOT_DELETE(408, "系统管理员不能删除"),
    NOT_DELETE(409, "当前用户不能删除"),
    NOT_SYS_MENU_DELETE(410, "系统菜单，不能删除"),
    NOT_SYS_MENU_DELETE_SUBMENU(411, "请先删除子菜单或按钮"),
    ID_LOCK(412, "账号已被锁定,请联系管理员"),
    SCHEDULE_ERROR(413, "定时任务异常"),
    HTTP_REQUEST_METHOD_NOT_SUPPORTED(414, "请查看api注意get/post等提交方式"),
    OSS_ERROR(430, "OSS异常"),
    FDFS_ERROR(431, "fdfs服务器异常,请联系管理员"),


    /**
     * 错误码定义类
     * 1000 - 9999 为模块错误码
     */

    SYSTEM_UNKNOWN_ERROR(1000, "系统未知错误."),
    REQUEST_PARAMS_INVALID(1001, "请求参数非法."),
    REQUEST_DECRYPT_FAIL(1002, "请求解密错误."),
    REQUEST_API_IS_NULL(1003, "请求接口为空"),
    REQUEST_NOT_AUTH(1004, "用户未登陆"),
    REQUEST_SIGN_INVALID(1005, "Sign校验失败"),
    REQUEST_PARAMS_NULL(1006, "请求参数为空."),
    REQUEST_PARAMS_UID_IS_NULL(1007, "uid参数为空."),
    PROTOCOL_IS_NULL(1010, "Protocol为空."),
    SECRET_KEY_LENGTH_WRONG(1011, "密钥长度不对."),
    USER_NOT_EXIST(1012, "用户不存在"),
    USER_ALREADY_EXIST(1013, "用户已经存在"),
    PHONE_ALREADY_EXIST(1014, "用户手机号已注册"),
    USER_NEED_LOGIN(1015, "用户需要登陆"),
    USER_SECRET_EXPIRE(1016, "秘钥过期"),
    DATE_FORMAT_ERROR(1017, "日期格式转换错误"),
    LEJANE_APPID_IS_ABSENT(1018, "APP_ID[{}]不存在"),
    REQUEST_TOKEN_EXPIRED(1019, "请求token过期"),
    IMEI_IN_BLACKLIST(1020, "黑名单设备"),
    CREATE_TOKEN_ERROR(1021, "创建token失败"),
    ID_NOT_NULL(1022, "id不能为空"),
    NAME_PASSWORD_ERROR(1023, "账号或密码不正确"),


    /**
     * 功能模块错误码定义
     */

    /**
     * 10000 - 10099 用户模块错误码
     */
    USER_NOT_EXIST_ERROR(10001, "用户名密码错误"),
    USER_PASSWORD_ERROR(10002, "密码错误"),
    USER_MOBILE_REPETITION(10003, "手机号重复"),
    USER_OLD_PASSWORD_ERROR(10004, "原密码不正确"),
    //USER_FAMILY_CHILD_ADD_COUNT_LIMIT_ERROR(10001, "xx增加数量超过上限"),
    //USER_FAMILY_CHILD_ADD_COUNT_ZERO_ERROR(10002, "xx增加数量为空"),
    //USER_PROPERTY_NOT_INIT(10003,"用户属性未初始化"),
    /** END 10000 - 10099 xxx模块错误码 */

    /**
     * 10100 - 10199 问卷模块错误码
     */
    QUESTIONNAIRE_SORT(10101, "投票无法查看统计"),
    QUESTIONNAIRE_SORT_ANSWER(10102, "参数类型错误,只有问卷才可以答题"),
    QUESTIONNAIRE_IS_NULL(10103, "问卷未找到请核对问卷id"),


    /**
     * 10200 - 10299 自主出题模块错误码
     */
    QUESTIONUSERADDID_IS_NULL(10201, "自主出题id不能为空"),
    QUESTIONUSERADD_USERID_IS_NULL(10202, "userId不能为空"),
    QUESTIONUSERADD_LIST_IS_NULL(10203, "该用户没有自主出题"),
    QUESTIONUSERADD_DETAILS_IS_NULL(10204, "查询不到出题详情,请核对自主出题id"),

    /**
     * 10300 - 10399 习题模块错误代码
     */
    QUESTION_COUNT(10300, "出题总数不能大于100"),
    QUESTION_NOT_DATA(10301, "没有获取到任何数据，请检查专业等参数是否正确"),
    QUESTION_EXERCISE_NOT_QUERY(10302, "练习试卷没找到，请检查参数是否正确"),
    QUESTION_EXERCISE_REPEATED_SUBMIT(10303, "此试卷已经提交，请勿重复提交！"),
    QUESTION_EXERCISE_NOT_NULL(10304, "习题不能为空！"),


    /**
     * 10400 - 10499 在线考试模块错误代码
     */
    NO_EXAMS_NOW(10400, "当前没有考试！"),
    NOT_REPEAT_EXAMS(10401, "不能重复进行考试"),
    NO_TEST_QUESTION(10402, "没有找到试题！"),
    PARAMETER_NOT_NULL(10403, "必传参数不能为空"),
    NOT_FIND_USER_TEST_PAPER(10404, "没有找到试卷，请检查参数是否正确！"),
    USER_TEST_PAPER_SUBMIT(10405, "用户试卷已提交，请勿重复提交！"),
    QUESTION_NOT_NULL(10306, "试题不能为空！"),
    TEST_PAPER_GIVE_UP(10307, "此试卷用户放弃考试！"),

    /**
     * 10500 - 10599 公告/通知模块错误码
     */
    ANNOUNCEMENT_ID_IS_NULL(10500, "公告id不能为空！"),
    ANNOUNCEMENT_IS_NULL(10501, "没有找到公告，请检查参数是否正确!"),
    ANNOUNCEMENT_USER_IS_NULL(10502, "没有找到用户，请检查参数是否正确!"),
    ANNOUNCEMENTCOMMEN_IS_NULL(10503, "没有找到评论，请检查参数是否正确!"),
    ANNOUNCEMENT_READ_LIST_IS_NULL(10504, "找不到已读列表，请检查参数是否正确！"),
    ANNOUNCEMENTCOMMEN_IS_LIKE(10505, "已经点过赞啦！"),
    ANNOUNCEMENTFORWARDURL_IS_NULL(10506, "暂时没有转发URL,请联系后台添加！"),


    /**
     * 10600 - 10699 公告/通知模块错误码
     */
    COURSE_TRAINING_USERID_IS_NULL(10600, "用户id不能为空！"),
    COURSE_TRAINING_LIST_IS_NULL(10601, "找不到相关的培训班信息，请检查参数！"),
    COURSE_WARE_TRAINING_LIST_IS_NULL(10602, "找不到相关的培训班课件信息，请检查参数！"),
    COURSE_TRAINING_ID_IS_NULL(10603, "培训班id不能为空！"),
    COURSE_ALREADY_TRAINED_USER(10604, "当前用户此课件已经学习过了！"),
    COURSE_TRAINING_USER_IS_NOT_INFO(10605, "当前用户不存在此培训班中!"),


    /**
     * 10700 - 10799 排班模块错误代码
     */
    ARRANGE_PARAMETER_NOT_NULL(10700, "必传参数不能为空"),
    ARRANGE_PARAMETER_ERROR(10701, "参数错误，请检查参数的正确性！"),
    ARRANGE_THE_RIGHT_TIME(10702, "请选择正确的初始化时间！"),
    ARRANGE_TIME_FORMAT_ERROR(10703, "时间格式转换错误，请检查传入的时间格式！"),
    ARRANGE_TIME_LIMIT_ERROR(10704, "调班日期必须大于等于今日且小于等于（今日+30）"),
    ARRANGE_NOT_FIND(10705, "没有找到指定的排班，请检查参数是否正确"),
    ARRANGE_APPLY_NOT_FIND(10706, "排班申请没有找到，请检查参数是否正确!"),
    ARRANGE_APPLY_DIFFERENT(10707, "申请班次必须和原班次不同!"),
    ARRANGE_APPLY_NOT_RESET(10708, "用户申请过调班或被申请过调班，不能进行排班重置!"),
    ARRANGE_TIME_ERROR(10709, "时间格式化错误，请联系管理员!"),
    ARRANGE_TIME_AFTER(10710, "不能申请白班，申请换班的前一天为后班，出现了'后白'的情况!"),
    ARRANGE_TIME_BEFOR(10711, "不能申请后班，申请换班的后一天为白班，出现了'后白'的情况!"),
    ARRANGE_WORK_APPLY_ING(10712, "不能调班，您的当前班次存在正在进行中的调班!"),
    ARRANGE_WORK_EXCHANGE_ING(10713, "不能调班，被换班次存在正在进行中的调班!"),


    /**
     * 20000 - 20099 web端 练习错误码
     */
    WEB_PARENTID_IS_NOT_NULL(20000, "必传参数不能为空"),


    ;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int code;
        private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


    private static Map<Integer, ErrorCode> errorMap = new HashMap<>();

    static {
        for (ErrorCode pec : ErrorCode.values()) {
            if (errorMap.containsKey(pec.code)) {
                throw new RuntimeException("error code[" + pec.code + "] ------com.guo.web.common.errorCode.ErrorCode");
            }
            errorMap.put(pec.getCode(), pec);
        }
    }

}
