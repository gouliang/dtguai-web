package com.dtguai.admin.common.log;

import com.alibaba.fastjson.JSON;
import com.dtguai.admin.common.annotation.SysLog;
import com.dtguai.admin.util.HttpContextUtils;
import com.dtguai.admin.util.IpUtils;
import com.dtguai.admin.web.sys.model.SysDefAdmin;
import com.dtguai.admin.web.sys.model.SysDefLog;
import com.dtguai.admin.web.sys.service.SysDefLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Optional;


/**
 * 系统日志，切面处理类
 *
 * @author guo
 * @date 2018年12月25日14:21:42
 */
@Aspect
@Component
@Slf4j
public class SysLogAspect {

    @Autowired
    private SysDefLogService sysDefLogService;

    @Pointcut("@annotation(com.dtguai.admin.common.annotation.SysLog)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();

        //执行方法
        Object result = point.proceed();

        //执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;

        //保存日志
        saveSysLog(point, time, result);

        return result;
    }

    /**
     * 保存自定义日志
     *
     * @param result body
     * @return 自定义日志json
     */
    private String saveOld(Object result) {
        return Optional.ofNullable(result)
                .map(JSON::toJSONString)
                .map(JSON::parseObject)
                .map(x -> x.get("body"))
                .map(Object::toString)
                .map(JSON::parseObject)
                .map(x -> x.get("oldLog"))
                .map(Object::toString)
                .orElse(null);
    }

    private void saveSysLog(ProceedingJoinPoint joinPoint, Long time, Object result) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        SysDefLog sysLog = new SysDefLog();
        SysLog syslog = method.getAnnotation(SysLog.class);
        if (syslog != null) {
            //注解上的描述
            sysLog.setOperation(syslog.value());
        }

        //请求的方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysLog.setMethod(className + "." + methodName + "()");

        //请求的参数
        Object[] args = joinPoint.getArgs();
        try {
            String params = JSON.toJSONString(args[0]);
            sysLog.setParams(params);
        } catch (Exception e) {
            log.error("log注解-请求的参数异常", e);
        }

        //获取request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        //设置IP地址
        sysLog.setIp(IpUtils.getIpAddress(request))
                //用户名
                .setUserName(Optional.ofNullable(SecurityUtils.getSubject())
                        .map(x -> (SysDefAdmin) x.getPrincipal())
                        .map(SysDefAdmin::getName)
                        .orElse(null))
                .setTime(time)
                .setCreateTime(new Date())
                //判断是否需要保存旧值
                .setOld(saveOld(result));

        //保存系统日志
        sysDefLogService.insertSelective(sysLog);
    }
}
