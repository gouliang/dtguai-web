package com.dtguai.admin.common.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * @author guo
 * @date 2019年7月9日15:14:08
 */
@Data
@Accessors(chain = true)
@Slf4j
@AllArgsConstructor
public class OldLogImpl implements OldLog {

    private Integer type;

    private Integer job;

    private String msg;


}
