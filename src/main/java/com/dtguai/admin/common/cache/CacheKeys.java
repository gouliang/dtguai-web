package com.dtguai.admin.common.cache;

import lombok.experimental.UtilityClass;

/**
 * 缓存对应键值
 *
 * @author guo
 * @date 2019年6月28日09:00:16
 */
@UtilityClass
public class CacheKeys {

    public static final String PROJECT_NAME = "dtguai:web:";

    public static final String AUTH_CODE_KEY = PROJECT_NAME + "kaptcha:key:";

}

