package com.dtguai.admin.common.xss;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author guo
 * @date 2018年9月11日上午10:34:14
 */
@Slf4j
public class XssFilter implements Filter {

    @Override
    public void init(FilterConfig config) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper((HttpServletRequest) request);
        try {
            chain.doFilter(xssRequest, response);
        } catch (IOException e) {
            log.error(this.getClass().getName() + ":IOException", e);
        } catch (ServletException e) {
            log.error(this.getClass().getName() + ":ServletException", e);
        }

    }

    @Override
    public void destroy() {
    }

}