package com.dtguai.admin.common.validator;

import java.util.List;
import org.apache.commons.lang.StringUtils;

import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.common.exception.DefinedException;

 /**
   * @author: guoLiang
   * @date：2018-09-29 03:46:03
   * @类描述：数据校验
   */
public abstract class BaseAssert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new DefinedException(message);
        }
    }
    
    public static void isNull(List<?> list, String message,ErrorCode ed) {
        if (list == null||list.size()==0) {
            throw new DefinedException(message,ed.getCode());
        }
    }
    
    public static void isNull(List<?> list,ErrorCode ed) {
    	isNull(list, ed.getMessage(), ed);
    }
    
    public static void isNull(Object object, ErrorCode ed) {
        if (object == null) {
        	throw new DefinedException(ed);
        }
    }
}