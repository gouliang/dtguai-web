package com.dtguai.admin.common.validator.group;

import javax.validation.GroupSequence;

/**
 * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
 *
 * @author guo
 * @date 2018年12月24日13:43:48
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {

}
