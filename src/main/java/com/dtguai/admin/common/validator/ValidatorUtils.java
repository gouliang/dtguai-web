package com.dtguai.admin.common.validator;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import com.dtguai.admin.common.exception.DefinedException;

 /**
   * @author guoLiang
   * @date 2018-09-29 03:46:03
   * @类描述 hibernate-validator校验工具类
   */
public class ValidatorUtils {
	private static Validator validator;

	static {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	/**
	 * 校验对象
	 * @param object        待校验对象
	 * @param groups        待校验的组
	 * @throws DefinedException  校验不通过，则报RRException异常
	 */
	public static void validateEntity(Object object, Class<?>... groups) {
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
		if (!constraintViolations.isEmpty()) {
			StringBuilder msg = new StringBuilder();
			for (ConstraintViolation<Object> constraint : constraintViolations) {
				msg.append(constraint.getMessage()).append("<br>");
			}
			throw new DefinedException(msg.toString());
		}
	}
}
