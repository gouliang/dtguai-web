package com.dtguai.admin.common.exception;

import com.dtguai.admin.common.error.ErrorCode;
import lombok.extern.slf4j.Slf4j;

/**
 * @author guoLiang
 * @date 2019年9月17日09:21:07
 */
@Slf4j
public class DefinedException extends RuntimeException {


    private String msg;

    private int code = ErrorCode.BAD_REQUEST.getCode();

    public DefinedException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public DefinedException(String msg, Throwable e) {
        super(msg, e);
        log.error(msg + e);
        this.msg = msg;
    }

    public DefinedException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public DefinedException(ErrorCode ed) {
        super(ed.getMessage());
        this.msg = ed.getMessage();
        this.code = ed.getCode();
    }

    public DefinedException(ErrorCode ed, String msg) {
        super(msg);
        this.msg = msg;
        this.code = ed.getCode();
    }

    public DefinedException(String msg, int code, Throwable e) {
        super(msg, e);
        log.error(msg + e);
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
