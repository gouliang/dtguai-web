package com.dtguai.admin.web.oss.form.oss;


import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * 文件上传
 *
 * @author guoLiang
 * @date 2019年7月2日16:25:46
 */
@Data
@ApiModel(value = "UploadForm")
public class UploadForm {


    /**
     * 文件file
     */
    @ApiModelProperty(value = "文件file", required = true)
    @NotNull(message = "文件file不能为空")
    @JSONField(serialize = false)
    private MultipartFile file;


    /**
     * 文档类型
     */
    @ApiModelProperty(value = "文档类型", required = true)
    @NotNull(message = "文档类型不能为空")
    @Range(min = 1, max = 99, message = "文档类型错误")
    private Integer documentType;


}