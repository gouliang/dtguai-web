package com.dtguai.admin.web.oss.model;

import com.dtguai.admin.page.PageModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 文件上传
 *
 * @author guo
 * @date 2019-07-11 13:30:40
 */
@Alias("sysDefOss")
@Table(name = "sys_def_oss")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class SysDefOss extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public SysDefOss() {
        super();
    }

    public SysDefOss(Integer id) {
        super();
        super.setId(id);
    }

    /**
     * URL地址
     */
    @ApiModelProperty(value = "URL地址")
    @Column(name = "url")
    private String url;


    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;


    /**
     * 文档类型
     */
    @ApiModelProperty(value = "文档类型")
    @Column(name = "document_type")
    private Integer documentType;


    /**
     * 1.7牛.oss3.奇瑞qq云
     */
    @ApiModelProperty(value = "1.7牛.oss3.奇瑞qq云")
    @Column(name = "type")
    private Integer type;


}