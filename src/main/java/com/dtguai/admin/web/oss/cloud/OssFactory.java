
package com.dtguai.admin.web.oss.cloud;


import com.dtguai.admin.util.constant.ConfigConstant;
import com.dtguai.admin.util.constant.Constant;
import com.dtguai.admin.util.SpringContextUtils;
import com.dtguai.admin.web.sys.service.SysDefConfigService;

/**
 * 文件上传Factory
 *
 * @author guo
 * @date 2018年12月25日11:25:15
 */
public final class OssFactory {
    private static SysDefConfigService sysConfigService;

    static {
        OssFactory.sysConfigService = (SysDefConfigService) SpringContextUtils.getBean("sysDefConfigService");
    }

    public static BaseCloudStorageService build() {
        //获取云存储配置信息
        CloudStorageConfig config = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);

        if (config.getType() == Constant.CloudService.QINIU.getValue()) {
            return new QiniuCloudStorageService(config);
        }else if(config.getType() == Constant.CloudService.ALIYUN.getValue()){
            return new AliyunCloudStorageService(config);
        }else if(config.getType() == Constant.CloudService.QCLOUD.getValue()){
            return new QcloudCloudStorageService(config);
        }

        return null;
    }

}
