package com.dtguai.admin.web.oss.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.oss.model.SysDefOss;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:02
   */
public interface SysDefOssDao extends BaseMapper<SysDefOss> {

	
}