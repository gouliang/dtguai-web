package com.dtguai.admin.web.oss.service.impl;

import com.dtguai.admin.base.service.impl.BaseServiceImpl;
import com.dtguai.admin.web.oss.model.SysDefOss;
import com.dtguai.admin.web.oss.service.SysDefOssService;
import org.springframework.stereotype.Service;

/**
 * @author: guoLiang
 * @date 2018-12-11 02:43:02
 */
@Service("sysDefOssService")
public class SysDefOssServiceImpl extends BaseServiceImpl<SysDefOss> implements SysDefOssService {

}