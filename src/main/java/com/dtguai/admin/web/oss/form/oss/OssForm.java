package com.dtguai.admin.web.oss.form.oss;


import com.dtguai.admin.base.form.BaseFrom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: guoLiang
 * @date：2018-12-19 02:34:49
 * @描述：文件上传
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "OssForm")
public class OssForm extends BaseFrom {


    /**
     * 文件上传主键
     */
    @ApiModelProperty(value = "文件上传主键")
    private Integer id;


    /**
     * URL地址
     */
    @ApiModelProperty(value = "URL地址")
    private String url;


    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}