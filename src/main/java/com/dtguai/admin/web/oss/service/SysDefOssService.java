package com.dtguai.admin.web.oss.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.oss.model.SysDefOss;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:02
   */
public interface SysDefOssService extends BaseService<SysDefOss> {

}