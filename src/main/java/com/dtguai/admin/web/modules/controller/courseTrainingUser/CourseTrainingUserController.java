package com.dtguai.admin.web.modules.controller.courseTrainingUser;


import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.common.annotation.SysLog;
import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.common.validator.BaseAssert;
import com.dtguai.admin.common.validator.ValidatorUtils;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.web.modules.form.courseTrainingUser.AddCourseTrainingUserForm;
import com.dtguai.admin.web.modules.form.courseTrainingUser.CourseTrainingUserForm;
import com.dtguai.admin.web.modules.model.CourseTrainingUser;
import com.dtguai.admin.web.modules.service.CourseTrainingUserService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户培训班
 *
 * @author guo
 * @date 2019-01-28 11:44:36
 */
@RestController
@RequestMapping("/web/modules/controller/courseTrainingUser")
@Api(value = "CourseTrainingUser-用户培训班-接口", tags = {"CourseTrainingUser-用户培训班-接口"})
@AllArgsConstructor
public class CourseTrainingUserController extends BaseAction {

    private final CourseTrainingUserService courseTrainingUserService;

    /**
     * 分页查询 注意返回信息是 PageInfo 自带分页所有信息
     *
     * @param courseTrainingUser 入参对象Form
     * @author guo
     * @date 2019-01-28 11:44:36
     */
    @ApiOperation(value = "分页查询-返回PageInfo", notes = "分页查询-返回PageInfo")
    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("modules:courseTrainingUser:list")
    public ResponseEntity<ApiResponse<PageInfo<CourseTrainingUser>>> list(CourseTrainingUserForm courseTrainingUser) {
        // 表单校验
        ValidatorUtils.validateEntity(courseTrainingUser);
        List<CourseTrainingUser> courseTrainingUserList = courseTrainingUserService.selectByPage(courseTrainingUser);
        return ResponseEntity.ok(new ApiResponse<>(new PageInfo<>(courseTrainingUserList)));
    }

    /**
     * 根据主键查询
     *
     * @param id 主键
     * @author guo
     * @date 2019-01-28 11:44:36
     */
    @ApiOperation(value = "根据主键查询", notes = "根据主键查询")
    @GetMapping(value = "/infoByKey", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 1022, message = "id不能为空")
    })
    @ApiImplicitParam(name = "id", value = "主键id", required = true, example = "1")
    @RequiresPermissions("modules:courseTrainingUser:infoByKey")
    public ResponseEntity<ApiResponse<CourseTrainingUser>> infoByKey(Integer id) {
        BaseAssert.isNull(id, ErrorCode.ID_NOT_NULL);
        return ResponseEntity.ok(new ApiResponse<>(courseTrainingUserService.selectByKey(id)));
    }

    /**
     * 添加数据 返回添加的id
     *
     * @param courseTrainingUser 入参对象Form
     * @author guo
     * @date 2019-01-28 11:44:36
     */
    @SysLog("添加-用户培训班")
    @ApiOperation(value = "添加", notes = "添加")
    @PostMapping(value = "/save", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("modules:courseTrainingUser:save")
    public ResponseEntity<ApiResponse<Integer>> save(@RequestBody AddCourseTrainingUserForm courseTrainingUser) {
        // 表单校验
        ValidatorUtils.validateEntity(courseTrainingUser);
        CourseTrainingUser courseTrainingUserAdd = new ModelMapper().map(courseTrainingUser, CourseTrainingUser.class);
        courseTrainingUserService.insertSelective(courseTrainingUserAdd);
        return ResponseEntity.ok(new ApiResponse<>(courseTrainingUserAdd.getId()));
    }

    /**
     * 更新数据
     *
     * @param courseTrainingUser 入参对象Form
     * @author guo
     * @date 2019-01-28 11:44:36
     */
    @SysLog("更新-用户培训班")
    @ApiOperation(value = "更新", notes = "更新")
    @PutMapping(value = "/update", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("modules:courseTrainingUser:update")
    public ResponseEntity<ApiResponse<Object>> update(@RequestBody CourseTrainingUserForm courseTrainingUser) {
        ValidatorUtils.validateEntity(courseTrainingUser, UpdateGroup.class);
        courseTrainingUserService.updateNotNull(new ModelMapper().map(courseTrainingUser, CourseTrainingUser.class));
        return ResponseEntity.ok(new ApiResponse<>());
    }

    /**
     * 根据主键删除数据
     *
     * @param ids 主键list
     * @author guo
     * @date 2019-01-28 11:44:36
     */
    @SysLog("删除-用户培训班")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping(value = "/delete", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 1022, message = "id不能为空")
    })
    @ApiImplicitParam(name = "ids", value = "主键id", required = true, example = "1,2,3,4,5")
    @RequiresPermissions("modules:courseTrainingUser:delete")
    public ResponseEntity<ApiResponse<Object>> delete(@RequestBody List<Integer> ids) {
        BaseAssert.isNull(ids, ErrorCode.ID_NOT_NULL);
        courseTrainingUserService.deleteByIds(ids, CourseTrainingUser.class);
        return ResponseEntity.ok(new ApiResponse<>());
    }

}