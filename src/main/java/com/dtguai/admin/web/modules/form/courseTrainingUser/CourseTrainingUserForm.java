package com.dtguai.admin.web.modules.form.courseTrainingUser;


import com.dtguai.admin.base.form.BaseFrom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import java.util.Date;

/**
 * 用户培训班
 *
 * @author: guo
 * @date：2019-01-28 11:44:36
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "CourseTrainingUserForm")
public class CourseTrainingUserForm extends BaseFrom {

 	

 	/**
	 * 用户id
	 */
	@ApiModelProperty(value = "用户id")
	private Integer userId;
 	

 	/**
	 * 培训班id
	 */
	@ApiModelProperty(value = "培训班id")
	private Integer courseTrainingId;
 	

 	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
 	

}