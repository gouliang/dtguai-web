package com.dtguai.admin.web.modules.model;

import com.dtguai.admin.page.PageModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户培训班
 *
 * @author: guo
 * @date：2019-01-28 11:44:36
 */
@Alias("courseTrainingUser")
@Table(name = "course_training_user")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class CourseTrainingUser extends PageModel implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	public CourseTrainingUser() {
		super();
	}
	
	public CourseTrainingUser(Integer id) {
		super();
		super.setId(id);
	}
	
	 		
	
	 /**
	* 用户id
	*/
	@ApiModelProperty(value = "用户id")
	@Column(name = "user_id")
	private Integer userId;
	 		
	
	 /**
	* 培训班id
	*/
	@ApiModelProperty(value = "培训班id")
	@Column(name = "course_training_id")
	private Integer courseTrainingId;
	 		
	
	 /**
	* 创建时间
	*/
	@ApiModelProperty(value = "创建时间")
	@Column(name = "create_time")
	private Date createTime;
	 		
		
}