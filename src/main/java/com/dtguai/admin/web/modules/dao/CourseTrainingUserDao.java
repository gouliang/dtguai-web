package com.dtguai.admin.web.modules.dao;

import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.modules.model.CourseTrainingUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户培训班
 * 
 * @author guo
 * @date 2019-01-28 11:44:36
 */
public interface CourseTrainingUserDao extends BaseMapper<CourseTrainingUser> {

    /**
     * 查询
     *
     * @param courseTrainingUser 入参对象
     * @return 返回xml的List<CourseTrainingUser>对象
     */
    List<CourseTrainingUser> selectByCourseTrainingUserModel(BaseFrom courseTrainingUser);

    /**
     *  Select标签 查询
     *
     * @param id id
     * @return CourseTrainingUser
     */
    @Select("select "
    		+ "id as id "
    	    	+ ",user_id as userId  "
    	+ ",course_training_id as courseTrainingId  "
    	+ ",create_time as createTime  "
    	    + "from course_training_user "
    		+ "where id = #{id}")
    CourseTrainingUser findCourseTrainingUserById(@Param("id") Integer id);

}