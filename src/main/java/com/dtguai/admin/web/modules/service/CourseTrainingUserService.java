package com.dtguai.admin.web.modules.service;

import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.modules.model.CourseTrainingUser;

import java.util.List;

/**
 * 用户培训班
 *
 * @author  guo
 * @date 2019-01-28 11:44:36
 */
public interface CourseTrainingUserService extends BaseService<CourseTrainingUser> {

	/**
	 * 使用xml 查询
	 *
	 * @param courseTrainingUser courseTrainingUser对象
	 * @return List<CourseTrainingUser>
	 */
	List<CourseTrainingUser> findCourseTrainingUserByXml(BaseFrom courseTrainingUser);

	/**
	 * Select标签 根据主键查询对象
	 *
	 * @param id id
	 * @return List<CourseTrainingUser>
	 */
	CourseTrainingUser findCourseTrainingUserById(Integer id);

}