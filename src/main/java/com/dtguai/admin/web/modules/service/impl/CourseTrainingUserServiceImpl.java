package com.dtguai.admin.web.modules.service.impl;

import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.base.service.impl.BaseServiceImpl;
import com.dtguai.admin.web.modules.dao.CourseTrainingUserDao;
import com.dtguai.admin.web.modules.model.CourseTrainingUser;
import com.dtguai.admin.web.modules.service.CourseTrainingUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户培训班
 *
 * @author guo
 * @date 2019-01-28 11:44:36
 */
@Service("courseTrainingUserService")
public class CourseTrainingUserServiceImpl extends BaseServiceImpl<CourseTrainingUser> implements CourseTrainingUserService {

    /**
     * 根据条件查询  自定义xml
     */
    @Override
    public List<CourseTrainingUser> findCourseTrainingUserByXml(BaseFrom courseTrainingUser) {
        return ((CourseTrainingUserDao) getMapper()).selectByCourseTrainingUserModel(courseTrainingUser);
    }

    /**
     * Select标签 根据主键查询对象
     */
    @Override
    public CourseTrainingUser findCourseTrainingUserById(Integer id) {
        return ((CourseTrainingUserDao) getMapper()).findCourseTrainingUserById(id);
    }


}