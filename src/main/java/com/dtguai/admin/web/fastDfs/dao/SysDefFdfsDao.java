package com.dtguai.admin.web.fastDfs.dao;

import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.fastDfs.model.SysDefFdfs;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * fdfs
 *
 * @author guo
 * @date 2019-06-28 17:32:43
 */
public interface SysDefFdfsDao extends BaseMapper<SysDefFdfs> {

    /**
     * 查询
     *
     * @param sysDefFdfs 入参对象
     * @return 返回xml的List<SysDefFdfs>对象
     */
    List<SysDefFdfs> selectBySysDefFdfsModel(BaseFrom sysDefFdfs);

    /**
     * Select标签 查询
     *
     * @param id id
     * @return SysDefFdfs
     */
    @Select("select "
            + "id as id "
            + ",name as name  "
            + ",size as size  "
            + ",group_name as groupName  "
            + ",fast_file_id as fastFileId  "
            + ",type as type  "
            + ",create_id as createId  "
            + ",create_time as createTime  "
            + "from sys_def_fdfs "
            + "where id = #{id}")
    SysDefFdfs findSysDefFdfsById(@Param("id") Integer id);

}