package com.dtguai.admin.web.fastDfs.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.fastDfs.model.SysDefFdfs;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;


/**
 * fdfs
 *
 * @author guo
 * @date 2019-06-28 17:32:43
 */
public interface SysDefFdfsService extends BaseService<SysDefFdfs> {

    SysDefFdfs upload(MultipartFile file);

    InputStream download(SysDefFdfs fdfs);

    int delete(SysDefFdfs attachment);

}