package com.dtguai.admin.web.fastDfs.form.sysDefFdfs;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * fdfs
 *
 * @author : guo
 * @date 2019-06-28 17:32:43
 */
@Data
@ApiModel(value = "AddSysDefFdfsForm")
public class AddSysDefFdfsForm {

    /**
     * 文件file
     */
    @ApiModelProperty(value = "要上传的文件", required = true)
    @NotNull(message = "文件file不能为空")
    @JSONField(serialize = false)
    private MultipartFile file;


    /**
     * 文档类型
     */
    @ApiModelProperty(value = "文档类型", required = true)
    @NotNull(message = "文档类型不能为空")
    @Range(min = 1, max = 99, message = "文档类型错误")
    private Integer documentType;


}