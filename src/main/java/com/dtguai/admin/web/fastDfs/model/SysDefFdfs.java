package com.dtguai.admin.web.fastDfs.model;

import com.dtguai.admin.page.PageModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * fdfs
 *
 * @author: guo
 * @date：2019-06-28 17:32:43
 */
@Alias("sysDefFdfs")
@Table(name = "sys_def_fdfs")
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class SysDefFdfs extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public SysDefFdfs() {
        super();
    }

    public SysDefFdfs(Integer id) {
        super();
        super.setId(id);
    }


    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    @Column(name = "name")
    private String name;


    /**
     * 大小
     */
    @ApiModelProperty(value = "大小")
    @Column(name = "size")
    private String size;


    /**
     * 组名
     */
    @ApiModelProperty(value = "组名")
    @Column(name = "group_name")
    private String groupName;


    /**
     * fdfs_id
     */
    @ApiModelProperty(value = "fdfs_id")
    @Column(name = "fast_file_id")
    private String fastFileId;


    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @Column(name = "type")
    private Integer type;


    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_id")
    private Integer createId;


    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * URL地址
     */
    @ApiModelProperty(value = "URL地址")
    @Column(name = "url")
    private String url;
}