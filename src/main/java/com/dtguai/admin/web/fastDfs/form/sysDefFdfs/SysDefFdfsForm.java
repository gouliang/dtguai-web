package com.dtguai.admin.web.fastDfs.form.sysDefFdfs;


import com.dtguai.admin.base.form.BaseFrom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * fdfs
 *
 * @author guo
 * @date 2019-06-28 17:32:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "SysDefFdfsForm")
public class SysDefFdfsForm extends BaseFrom {


    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;


    /**
     * 大小
     */
    @ApiModelProperty(value = "大小")
    private String size;


    /**
     * 组名
     */
    @ApiModelProperty(value = "组名")
    private String groupName;


    /**
     * fdfs_id
     */
    @ApiModelProperty(value = "fdfs_id")
    private String fastFileId;


    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private Integer type;


    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    private Integer createId;


    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}