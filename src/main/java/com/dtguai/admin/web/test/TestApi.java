package com.dtguai.admin.web.test;


import com.alibaba.fastjson.JSON;
import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.common.annotation.SysLog;
import com.dtguai.admin.common.log.OldLogImpl;
import com.dtguai.admin.common.sign.SignAspect;
import com.dtguai.admin.config.EncryptBodyConfig;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.util.security.RsaEncryptUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

/**
 * 样例Api
 *
 * @author guo
 * @date 2019年6月25日14:58:38
 */
@RestController
@RequestMapping("/api/test")
@Api(value = "test", tags = {"test"})
@Slf4j
public class TestApi extends BaseAction {

    @Autowired
    private EncryptBodyConfig config;

    @Value("${sign.key:#{null}}")
    private String signKey;


    /**
     * 测试的json
     * {  "name": "admin",  "password": "admin", "captcha": "2a3xn",  "uuid": "7617e8c9-8828-47d0-80e9-9473a23c0bb6"}
     *
     * @param json 需要加密的json
     * @return Test 加密数据 数字证书 加密使用的时间戳
     */
    @ApiOperation(value = "根据json获取sign和dataSecret", notes = "根据json获取sign和dataSecret")
    @PostMapping(value = "/sd", produces = "application/json;charset=UTF-8")
    @ApiModelProperty(name = "msg", value = "需要搞一搞的json", required = true)
    public ResponseEntity<com.dtguai.admin.protocol.response.ApiResponse<Test>> sd(@RequestBody String json) {

        log.warn(json);

        Map my = JSON.parseObject(json, TreeMap.class);
        Object timestamp = Optional.ofNullable(my)
                .map(x -> x.get(SignAspect.TIMESTAMP_HEADER))
                .orElse(System.currentTimeMillis());
        my.put(SignAspect.TIMESTAMP_HEADER, timestamp);

        log.warn(JSON.toJSONString(my));
        //公钥加密
        byte[] jiami = RsaEncryptUtil.encryptByPublicKey(JSON.toJSONString(my).getBytes(), Base64.decodeBase64(config.getRsaPubKey()));

        StringBuilder paramBuilder = new StringBuilder();
        my.forEach((k, v) -> {
            if (v != null && !k.equals(SignAspect.SIGN_HEADER) && !k.equals(SignAspect.TOKEN_HEADER)) {
                paramBuilder.append(k).append("=").append(v).append("&");
            }
        });
        String signData = paramBuilder.append("signKey=").append(signKey).toString();
        log.warn(signKey);
        Test test = new Test();
        test.setDataSecret(Base64.encodeBase64String(jiami))
                .setSign(DigestUtils.md5Hex(signData))
                .setTimestamp(timestamp.toString());
        return ResponseEntity.ok(new ApiResponse<>(test));
    }


    @ApiOperation(value = "自定义日志", notes = "自定义日志")
    @GetMapping(value = "/log", produces = "application/json;charset=UTF-8")
    @ApiModelProperty(name = "msg", value = "msg", required = false)
    @SysLog("测试操作")
    public ResponseEntity<ApiResponse<String>> testLog(String msg) {
        log.info(msg);
        msg = "插入数据操作 返回id=1";
        return ResponseEntity.ok(new ApiResponse<>(msg, new OldLogImpl(1, 2, msg)));
    }
}