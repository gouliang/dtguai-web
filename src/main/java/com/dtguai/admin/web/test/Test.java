package com.dtguai.admin.web.test;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author guo
 * @date 2019年7月19日15:49:30
 */
@Data
@Accessors(chain = true)
public class Test {

    /**
     * 加密数据
     */
    @ApiModelProperty(value = "加密数据")
    private String dataSecret;
    /**
     * 数字签名
     */
    @ApiModelProperty(value = "数字签名")
    private String sign;
    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳")
    private String timestamp;
}