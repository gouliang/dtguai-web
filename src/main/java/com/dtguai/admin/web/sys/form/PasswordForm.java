
package com.dtguai.admin.web.sys.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 密码表单
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.4.0 2018-01-25
 */
@Data
@ApiModel(value = "PasswordForm")
public class PasswordForm {
    /**
     * 原密码
     */
    @ApiModelProperty(value = "原密码",example="123456",required=true)
    @NotBlank(message="原密码不能为空")
    @Length(min=1, max=50,message="新密码长度不能超过50")
    private String password;
    /**
     * 新密码
     */
    @ApiModelProperty(value = "新密码",example="123456",required=true)
    @NotBlank(message="新密码不能为空")
    @Length(min=1, max=50,message="新密码长度不能超过50")
    private String newPassword;
}
