package com.dtguai.admin.web.sys.controller.log;


import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.web.sys.form.SysDefLogForm;
import com.dtguai.admin.web.sys.model.SysDefLog;
import com.dtguai.admin.web.sys.service.SysDefLogService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author guoLiang
 * @date 2018-12-11 02:43:02
 */
@RestController
@RequestMapping("/web/sys/controller/log")
@Api(value = "web-系统日志接口", tags = {"web-系统日志接口"})
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SysDefLogApi extends BaseAction {

    private SysDefLogService sysDefLogService;

    /**
     * 分页查询log
     *
     * @param sysDefLog 入参对象
     * @return ResponseEntity<PageInfo < SysDefLog>>
     * @author guoLiang
     */
    @ApiOperation(value = "分页查询-返回PageInfo", notes = "分页查询-返回PageInfo")
    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:log:list")
    public ResponseEntity<ApiResponse<PageInfo<SysDefLog>>> sysDefLogPage(SysDefLogForm sysDefLog) {
        List<SysDefLog> sysDefLogList = sysDefLogService.selectByPage(sysDefLog, true, SysDefLog.class);
        return ResponseEntity.ok(new ApiResponse<PageInfo<SysDefLog>>(new PageInfo<SysDefLog>(sysDefLogList)));
    }

}