package com.dtguai.admin.web.sys.service.impl;

import com.dtguai.admin.base.service.impl.BaseServiceImpl;
import com.dtguai.admin.web.sys.model.SysDefLog;
import com.dtguai.admin.web.sys.service.SysDefLogService;
import org.springframework.stereotype.Service;

/**
 * @author guoLiang
 * @date 2018-12-11 02:43:02
 */
@Service("sysDefLogService")
public class SysDefLogServiceImpl extends BaseServiceImpl<SysDefLog> implements SysDefLogService {


}