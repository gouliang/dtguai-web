package com.dtguai.admin.web.sys.form.admin;


import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.base.form.BaseFrom;

/**
 * @author: guoLiang
 * @date：2018-12-18 01:40:21
 * @描述：系统用户
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "ListAdminForm")
public class ListAdminForm extends BaseFrom {


    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String name;

    /**
     * 性别0女1男
     */
    @ApiModelProperty(value = "性别0女1男")
    private Integer sex;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 状态  0：禁用   1：正常
     */
    @ApiModelProperty(value = "状态  0：禁用   1：正常")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 创建者ID
     */
    @ApiModelProperty(value = "创建者ID")
    private Integer createAdminId;
}