package com.dtguai.admin.web.sys.model;

import com.dtguai.admin.page.PageModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色
 *
 * @author: guoLiang
 * @date 2018-12-11 01:57:09
 */
@Alias("sysDefRole")
@Table(name = "sys_def_role")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Accessors(chain = true)
public class SysDefRole extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public SysDefRole() {
        super();
    }

    public SysDefRole(Integer id) {
        super();
        super.setId(id);
    }

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    @Column(name = "role_name")
    private String roleName;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;
    /**
     * 创建者ID
     */
    @ApiModelProperty(value = "创建者ID")
    @Column(name = "create_admin_id")
    private Integer createAdminId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 更新人ID
     */
    @ApiModelProperty(value = "更新人ID")
    @Column(name = "update_admin_id")
    private Integer updateAdminId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "菜单id列表")
    @Transient
    private List<Integer> menuIdList;

}