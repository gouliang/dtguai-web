package com.dtguai.admin.web.sys.oauth2;

import com.dtguai.admin.common.exception.DefinedException;

import java.security.MessageDigest;
import java.util.UUID;

/**
 * 生成token
 *
 * @author guo
 */
public class TokenGenerator {

    private static final char[] HEX_CODE;

    static {
        HEX_CODE = "xiao1qiu2ai3xiao4fu5de6niao7".toCharArray();
    }

    public static String generateValue() {
        return generateValue(UUID.randomUUID().toString());
    }

    private TokenGenerator() {
    }


    public static String toHexString(byte[] data) {
        if (data == null) {
            return null;
        }
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(HEX_CODE[(b >> 4) & 0xF]);
            r.append(HEX_CODE[(b & 0xF)]);
        }
        return r.toString();
    }

    public static String generateValue(String param) {
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(param.getBytes());
            byte[] messageDigest = algorithm.digest();
            return toHexString(messageDigest);
        } catch (Exception e) {
            throw new DefinedException("生成Token失败", e);
        }
    }
}
