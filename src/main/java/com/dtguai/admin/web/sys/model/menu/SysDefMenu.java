package com.dtguai.admin.web.sys.model.menu;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.ibatis.type.Alias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.page.PageModel;

import java.util.List;

/**
 * @author: guoLiang
 * @date：2018-12-11 01:57:05
 * @描述：菜单管理
 */
@Alias("sysDefMenu")
@Table(name = "sys_def_menu")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class SysDefMenu extends PageModel implements Serializable {

	private static final long serialVersionUID = 1L;

	public SysDefMenu() {
		super();
	}

	public SysDefMenu(Integer id) {
		super();
		super.setId(id);
	}

	/**
	 * 父菜单ID，一级菜单为0
	 */
	@ApiModelProperty(value = "父菜单ID，一级菜单为0")
	@Column(name = "parent_id")
	private Integer parentId;
	/**
	 * 菜单名称
	 */
	@ApiModelProperty(value = "菜单名称")
	@Column(name = "name")
	private String name;
	/**
	 * 菜单URL
	 */
	@ApiModelProperty(value = "菜单URL")
	@Column(name = "url")
	private String url;
	/**
	 * 授权(多个用逗号分隔，如：user:list,user:create)
	 */
	@ApiModelProperty(value = "授权(多个用逗号分隔，如：user:list,user:create)")
	@Column(name = "perms")
	private String perms;
	/**
	 * 类型   0：目录   1：菜单   2：按钮
	 */
	@ApiModelProperty(value = "类型   0：目录   1：菜单   2：按钮")
	@Column(name = "type")
	private Integer type;
	/**
	 * 菜单图标
	 */
	@ApiModelProperty(value = "菜单图标")
	@Column(name = "icon")
	private String icon;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	@Column(name = "orders")
	private Integer orders;

	@Transient
	private List<?> list;

	/**
	 * 父菜单名称
	 */
	@Transient
	@ApiModelProperty(value = "父菜单名称")
	private String parentName;

	/**
	 * ztree属性
	 */
	@Transient
	private Boolean open;
}