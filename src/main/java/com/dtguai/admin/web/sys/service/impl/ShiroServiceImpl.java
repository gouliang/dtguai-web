package com.dtguai.admin.web.sys.service.impl;

import com.alibaba.fastjson.JSON;
import com.dtguai.admin.util.constant.Constant;
import com.dtguai.admin.util.redis.RedisUtil;
import com.dtguai.admin.web.sys.model.SysAdminToken;
import com.dtguai.admin.web.sys.model.SysDefAdmin;
import com.dtguai.admin.web.sys.model.menu.SysDefMenu;
import com.dtguai.admin.web.sys.service.ShiroService;
import com.dtguai.admin.web.sys.service.SysDefAdminService;
import com.dtguai.admin.web.sys.service.SysDefMenuService;
import com.dtguai.admin.web.sys.service.SysUserTokenService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * ShiroServiceImpl
 *
 * @author guo
 * @date 2019年1月23日11:22:19
 */
@Service
public class ShiroServiceImpl implements ShiroService {

    @Autowired
    private SysDefMenuService sysDefMenuService;

    @Autowired
    private SysDefAdminService sysDefAdminService;

    @Autowired
    private SysUserTokenService sysUserTokenService;

    @Autowired
    RedisUtil redisUtil;

    public final static String WEB_SHIRO_PERMISSIONS = "web:shiro:permissions:";

    public final static String WEB_SHIRO_ADMIN_INFO = "web:shiro:admin:info:";

    @Override
    public Set<String> getUserPermissions(Integer adminId) {
        List<String> permsList;

        String key = WEB_SHIRO_PERMISSIONS + adminId;

        Set<String> permsSet = redisUtil.getModel(key, Set.class);
        if (null != permsSet && permsSet.size() > 0) {
            return permsSet;
        }

        //系统管理员，拥有最高权限
        if (adminId == Constant.SUPER_ADMIN) {
            List<SysDefMenu> menuList = sysDefMenuService.selectByPage(new SysDefMenu(), false, SysDefMenu.class);
            permsList = new ArrayList<>(menuList.size());
            for (SysDefMenu menu : menuList) {
                permsList.add(menu.getPerms());
            }
        } else {
            permsList = sysDefAdminService.getAdminPermsList(adminId);
        }
        //用户权限列表
        permsSet = new HashSet<>();
        for (String perms : permsList) {
            if (StringUtils.isBlank(perms)) {
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }

        redisUtil.setString(key, JSON.toJSONString(permsSet));

        return permsSet;
    }

    @Override
    public SysAdminToken getAdminToken(String token) {
        return sysUserTokenService.getToken(token);
    }

    @Override
    public SysDefAdmin queryUser(Integer userId) {
        String key = WEB_SHIRO_ADMIN_INFO + userId;
        SysDefAdmin admin = redisUtil.getModel(key, SysDefAdmin.class);
        if (null == admin) {
            admin = sysDefAdminService.selectByKey(userId);
            redisUtil.setString(key, JSON.toJSONString(admin));
        }
        return admin;
    }

    @Override
    public void refreshTokenTime(SysAdminToken tokenEntity) {
        sysUserTokenService.refreshTokenTime(tokenEntity.getToken());
    }
}
