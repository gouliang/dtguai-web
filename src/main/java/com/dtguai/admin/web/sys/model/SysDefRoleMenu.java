package com.dtguai.admin.web.sys.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.ibatis.type.Alias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.page.PageModel;

/**
   * @author: guoLiang
   * @date：2018-12-11 01:57:10
   * @描述：角色与菜单对应关系
   */	
@Alias("sysDefRoleMenu")
@Table(name = "sys_def_role_menu")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class SysDefRoleMenu extends PageModel implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	public SysDefRoleMenu() {
		super();
	}
	
	public SysDefRoleMenu(Integer id) {
		super();
		super.setId(id);
	}
	
	/**
	 * 角色ID
	 */							
	@ApiModelProperty(value = "角色ID")
	@Column(name = "sys_def_role_id")
	private Integer sysDefRoleId;
	/**
	 * 菜单ID
	 */							
	@ApiModelProperty(value = "菜单ID")
	@Column(name = "sys_def_menu_id")
	private Integer sysDefMenuId;

}