package com.dtguai.admin.web.sys.controller;

import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.common.validator.ValidatorUtils;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.web.sys.form.SysLoginForm;
import com.dtguai.admin.web.sys.form.TestLoginForm;
import com.dtguai.admin.web.sys.model.SysAdminToken;
import com.dtguai.admin.web.sys.model.SysDefAdmin;
import com.dtguai.admin.web.sys.service.SysCaptchaService;
import com.dtguai.admin.web.sys.service.SysDefAdminService;
import com.dtguai.admin.web.sys.service.SysUserTokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * 登录相关
 *
 * @author guo
 * @date 2018年12月21日11:16:27
 */
@RestController
@RequestMapping("/web")
@Api(value = "web-登陆-登出", tags = {"web-登陆-登出"})
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SysLoginController extends BaseAction {

    private SysDefAdminService sysDefAdminService;

    private SysUserTokenService sysUserTokenService;

    private SysCaptchaService sysCaptchaService;

    /**
     * 登录
     */
    @ApiOperation(value = "web登陆", notes = "web登陆")
    @PostMapping(value = "/login", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 402, message = "验证码错误"),
            @io.swagger.annotations.ApiResponse(code = 412, message = "账号已被锁定,请联系管理员")
    })
    //@Sign
    //@RSADecryptBody(timeOut = 60L * 60L * 1000L)
    public ResponseEntity<ApiResponse<SysAdminToken>> login(@RequestBody SysLoginForm form) {
        if (!sysCaptchaService.validate(form.getUuid(), form.getCaptcha())) {
            return ResponseEntity.ok(new ApiResponse<>(ErrorCode.VERIFICATION_CODE_LOSE_EFFICACY));
        }
        // 表单校验
        ValidatorUtils.validateEntity(form);

        //根据用户名查询用户信息
        List<SysDefAdmin> adminList = sysDefAdminService.selectByPage(new SysDefAdmin().setName(form.getName()), true, SysDefAdmin.class);

        //账号不存在、密码错误
        if (adminList == null || adminList.size() != 1 || !adminList.get(0).getPassword().equals(new Sha256Hash(form.getPassword(), adminList.get(0).getCode()).toHex())) {
            return ResponseEntity.ok(new ApiResponse<>(ErrorCode.NAME_PASSWORD_ERROR));
        }

        //账号锁定
        if (adminList.get(0).getStatus() == 0) {
            return ResponseEntity.ok(new ApiResponse<>(ErrorCode.ID_LOCK));
        }

        //生成token，并保存到缓存
        SysAdminToken token = sysUserTokenService.createToken(adminList.get(0).getId());
        //返回token 及 过期时间
        return ResponseEntity.ok(new ApiResponse<>(token));
    }


    /**
     * 测试专用登录
     */
    @ApiOperation(value = "测试专用登录", notes = "测试专用登录")
    @PostMapping(value = "/test/login/", produces = "application/json;charset=UTF-8")
    public ResponseEntity<ApiResponse<SysAdminToken>> loginTest(@RequestBody TestLoginForm form) {
        //根据用户名查询用户信息
        List<SysDefAdmin> adminList = sysDefAdminService
                .selectByPage(new SysDefAdmin().setName(form.getName()), true, SysDefAdmin.class);

        //生成token，并保存到缓存
        SysAdminToken token = sysUserTokenService.createToken(adminList.get(0).getId());
        //返回token 及 过期时间
        return ResponseEntity.ok(new ApiResponse<>(token));
    }

    /**
     * 退出
     */
    @PostMapping("/logout")
    @ApiOperation(value = "web logout")
    public ResponseEntity<ApiResponse> logout() {
        sysUserTokenService.logout(getAdminId());
        return ResponseEntity.ok(new ApiResponse());
    }

    /**
     * 验证码
     */
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid) throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //获取图片验证码
        BufferedImage image = sysCaptchaService.getCaptcha(uuid);

        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
        IOUtils.closeQuietly(out);
    }

}
