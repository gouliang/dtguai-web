package com.dtguai.admin.web.sys.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 系统用户Token
 *
 * @author guo
 */
@Data
@Accessors(chain = true)
public class SysAdminToken implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Integer adminId;

    /**
     * token
     */
    private String token;

    /**
     * 过期时间
     */
    private Date expireTime;

    /**
     * 过期时间
     */
    private Integer expire;

    /**
     * 更新时间
     */
    private Date updateTime;


}
