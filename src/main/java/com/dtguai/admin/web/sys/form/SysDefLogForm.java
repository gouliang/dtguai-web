package com.dtguai.admin.web.sys.form;


import com.dtguai.admin.base.form.BaseFrom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

 /**
   * @author: guoLiang
   * @date：2018-12-18 01:40:22
   * @描述：系统日志
   */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "SysDefLogForm")
public class SysDefLogForm extends BaseFrom{



	/**
	 * 系统日志主键
	 */
	@ApiModelProperty(value = "系统日志主键")
	private Integer id;


	/**
	 * 用户名
	 */							
	@ApiModelProperty(value = "用户名")
	private String userName;


	/**
	 * 用户操作
	 */							
	@ApiModelProperty(value = "用户操作")
	private String operation;


	/**
	 * 请求方法
	 */							
	@ApiModelProperty(value = "请求方法")
	private String method;


	/**
	 * 请求参数
	 */							
	@ApiModelProperty(value = "请求参数")
	private String params;


	/**
	 * 执行时长(毫秒)
	 */							
	@ApiModelProperty(value = "执行时长(毫秒)")
	private Integer time;


	/**
	 * IP地址
	 */							
	@ApiModelProperty(value = "IP地址")
	private String ip;


	/**
	 * 创建时间
	 */							
	@ApiModelProperty(value = "创建时间")
	private Date createTime;


}