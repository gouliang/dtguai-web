

package com.dtguai.admin.web.sys.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录表单
 *
 * @author guo
 * @date 2019年1月23日11:22:19
 */
@Data
@ApiModel(value = "SysLoginForm")
public class SysLoginForm {

    @ApiModelProperty(value = "用户名",example = "admin",hidden = true,required = true)
    @NotBlank(message = "用户名不能为空")
    private String name;

    @ApiModelProperty(value = "密码",example = "admin",hidden = true,required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "验证码",hidden = true,required = true)
    @NotBlank(message = "验证码不能为空")
    private String captcha;

    @ApiModelProperty(value = "uuid",hidden = true,required = true)
    @NotBlank(message = "uuid不能为空")
    private String uuid;

    @ApiModelProperty(value = "当前时间戳",hidden = true)
    private String timestamp;

    @ApiModelProperty(value = "数字签名", example = "f45093c34df858ef779c73a16b53a975")
    private String sign;

    @ApiModelProperty(value = "加密数据", example = "m88Z27+lwYkHsOwPFZmrBV2QdaEo4GHlkZcb7kj3Qll2nHYuWQUpvXGKN5j9yYrvueM7yV31YJVn4P+/nlsfx4/b2wvDMwqIogo1FoJfO8M4FOvY1wTSf4ja3MrcoCui7At2YUZ9NdNgqvI59cQ3Xo2GXIHWj2d4+wx9aimJaN0bxYNSGC+P5wvS3xCT8wK5al8Vh7UIM0T5L/RW4nBIVuzX+yGsWdWuDBuQ/IrTiJr6DquVJPpWdAX+pgGxTN46Lbnd47y9X1BoWKXfLo8lO/MV3Ql8428rlkHk3l7I7DPdgIvdNoOfb8B/3oIZCAbmdgc43clJWSki53wn7k/q2w==")
    private String dataSecret;
}
