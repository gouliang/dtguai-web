package com.dtguai.admin.web.sys.form.admin;


import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.common.validator.group.AddGroup;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author: guoLiang
 * @date：2018-12-18 01:40:21
 * @描述：系统用户
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "SaveAdminForm")
public class SaveAdminForm extends BaseFrom {


    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    @NotBlank(message = "用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    @NotBlank(message = "密码不能为空", groups = AddGroup.class)
    private String password;

    /**
     * 性别0女1男
     */
    @ApiModelProperty(value = "性别0女1男")
    private Integer sex;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    @NotBlank(message = "邮箱不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @Email(message = "邮箱格式不正确", groups = {AddGroup.class, UpdateGroup.class})
    private String email;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 状态  0：禁用   1：正常
     */
    @ApiModelProperty(value = "状态  0：禁用   1：正常")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remake;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private List<Integer> roleIdList;
}