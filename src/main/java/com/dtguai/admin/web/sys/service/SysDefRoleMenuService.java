package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.page.PageModel;
import com.dtguai.admin.web.sys.model.SysDefRoleMenu;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:07
   */
public interface SysDefRoleMenuService extends BaseService<SysDefRoleMenu> {

    /**
     * 根据条件查询
     * @param sysDefRoleMenu sysDefRoleMenu
     * @return List<SysDefRoleMenu>
     */
    List<SysDefRoleMenu> selectBySysDefRoleMenu(PageModel sysDefRoleMenu);

    /**
     * 根据条件查询
     * @param sysDefRoleMenu sysDefRoleMenu
     * @param page page
     * @param clas clas
     * @return List<SysDefRoleMenu>
     */
    List<SysDefRoleMenu> selectBySysDefRoleMenuPage(PageModel sysDefRoleMenu, boolean page, Class<?> clas);

    /**
     * saveOrUpdate
     * @param roleId roleId
     * @param menuIdList menuIdList
     */
    @Transactional(rollbackFor = Exception.class)
    void saveOrUpdate(Integer roleId, List<Integer> menuIdList);

    /**
     * 根据角色ID，获取菜单ID列表
     * @param roleId roleId
     * @return List<Integer>
     */
    List<Integer> getMenuIdList(Integer roleId);

    /**
     * 根据角色ID数组，批量删除
     * @param roleIds roleIds
     * @return Integer
     */
    Integer deleteBatchById(List<Integer> roleIds);
}