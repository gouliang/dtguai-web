package com.dtguai.admin.web.sys.form.role;


import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.common.validator.group.AddGroup;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
   * @author: guoLiang
   * @date：2018-12-19 02:34:54
   * @描述：角色
   */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@ApiModel(value = "RoleForm")
public class RoleForm extends BaseFrom{





	/**
	 * 角色名称
	 */							
	@ApiModelProperty(value = "角色名称")
	@NotBlank(message = "角色名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String roleName;


	/**
	 * 备注
	 */							
	@ApiModelProperty(value = "备注")
	private String remark;


	/**
	 * 创建者ID
	 */							
	@ApiModelProperty(value = "创建者ID")
	private Integer createAdminId;

	/**
	 * 菜单id-list
	 */
	@ApiModelProperty(value = "菜单id-list")
	private List<Integer> menuIdList;
}