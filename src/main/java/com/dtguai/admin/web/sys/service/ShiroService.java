package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.web.sys.model.SysAdminToken;
import com.dtguai.admin.web.sys.model.SysDefAdmin;

import java.util.Set;

/**
 * shiro相关接口
 *
 * @author guo
 */
public interface ShiroService {

    /**
     * 获取用户权限列表
     *
     * @param adminId
     * @return Set<String>
     */
    Set<String> getUserPermissions(Integer adminId);

    /**
     * 根据用户token，查询用户
     *
     * @param token
     * @return SysAdminToken
     */
    SysAdminToken getAdminToken(String token);

    /**
     * 根据用户ID，查询用户
     *
     * @param id
     * @return SysDefAdmin
     */
    SysDefAdmin queryUser(Integer id);

    /**
     * 刷新token
     *
     * @param tokenEntity 用户token实体
     */
    void refreshTokenTime(SysAdminToken tokenEntity);
}
