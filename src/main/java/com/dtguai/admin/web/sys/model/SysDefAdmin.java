package com.dtguai.admin.web.sys.model;

import com.dtguai.admin.page.PageModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统用户
 *
 * @author: guoLiang
 * @date 2018-12-11 01:57:04
 */
@Alias("sysDefAdmin")
@Table(name = "sys_def_admin")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Accessors(chain = true)
public class SysDefAdmin extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public SysDefAdmin() {
        super();
    }

    public SysDefAdmin(Integer id) {
        super();
        super.setId(id);
    }

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    @Column(name = "name")
    private String name;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    @Column(name = "password")
    private String password;
    /**
     * 随即串
     */
    @ApiModelProperty(value = "随即串")
    @Column(name = "code")
    private String code;
    /**
     * 性别0女1男
     */
    @ApiModelProperty(value = "性别0女1男")
    @Column(name = "sex")
    private Integer sex;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    @Column(name = "email")
    private String email;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    @Column(name = "mobile")
    private String mobile;
    /**
     * 状态  0：禁用   1：正常
     */
    @ApiModelProperty(value = "状态  0：禁用   1：正常")
    @Column(name = "status")
    private Integer status;
    /**
     * 创建者ID
     */
    @ApiModelProperty(value = "创建者ID")
    @Column(name = "create_admin_id")
    private Integer createAdminId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @Column(name = "remake")
    private String remake;

    /**
     * 角色ID列表
     */
    @Transient
    @ApiModelProperty(value = "角色ID列表")
    private List<Integer> roleIdList;

}