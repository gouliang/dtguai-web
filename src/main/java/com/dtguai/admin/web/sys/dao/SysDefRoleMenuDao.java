package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.SysDefRoleMenu;

/**
 * @author guoLiang
 * @date 2019年11月4日13:49:53
 */
public interface SysDefRoleMenuDao extends BaseMapper<SysDefRoleMenu> {

}