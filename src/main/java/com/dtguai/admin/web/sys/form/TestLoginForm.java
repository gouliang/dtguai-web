

package com.dtguai.admin.web.sys.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录表单
 *
 * @author guo
 * @date 2019年1月23日11:22:19
 */
@Data
@ApiModel(value = "TestLoginForm")
public class TestLoginForm {

    @ApiModelProperty(value = "用户名",example = "admin",required = true)
    @NotBlank(message = "用户名不能为空")
    private String name;

    @ApiModelProperty(value = "密码",example = "admin",required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

}
