package com.dtguai.admin.web.sys.oauth2;


import org.apache.shiro.authc.AuthenticationToken;

/**
 * Oauth2Token
 *
 * @author guo
 */
public class Oauth2Token implements AuthenticationToken {

    private static final long serialVersionUID = 1L;

    private String token;

    public Oauth2Token(String token) {
        this.token = token;
    }

    @Override
    public String getPrincipal() {
        return getToken();
    }

    @Override
    public Object getCredentials() {
        return getToken();
    }

    private String getToken() {
        return token;
    }
}
