package com.dtguai.admin.web.sys.form.config;


import com.dtguai.admin.common.validator.group.AddGroup;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.base.form.BaseFrom;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
   * @author: guoLiang
   * @date：2018-12-19 02:34:48
   * @描述：系统配置信息表
   */
@Data
@ApiModel(value = "ConfigForm")
@EqualsAndHashCode(callSuper = true)
public class ConfigForm extends BaseFrom{



	/**
	 * 系统配置信息主键
	 */							
	@ApiModelProperty(value = "系统配置信息主键")
	@NotNull(message = "id不能为空", groups = {UpdateGroup.class})
	private Integer id;


	/**
	 * key
	 */							
	@ApiModelProperty(value = "key")
	@NotBlank(message = "key不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String paramKey;


	/**
	 * value
	 */							
	@ApiModelProperty(value = "value")
	@NotBlank(message = "value不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String paramValue;


	/**
	 * 状态   0：隐藏   1：显示
	 */							
	@ApiModelProperty(value = "状态   0：隐藏   1：显示")
	private Integer status;


	/**
	 * 备注
	 */							
	@ApiModelProperty(value = "备注")
	private String remark;


}