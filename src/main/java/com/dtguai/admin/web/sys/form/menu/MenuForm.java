package com.dtguai.admin.web.sys.form.menu;

import com.dtguai.admin.base.form.BaseFrom;
import com.dtguai.admin.common.validator.group.AddGroup;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author: guoLiang
 * @date：2018年12月20日14:30:53
 * @描述：菜单管理form
 */
@Data
@ApiModel(value = "MenuForm")
@EqualsAndHashCode(callSuper = true)
public class MenuForm extends BaseFrom {

    /**
     * 父菜单ID，一级菜单为0
     */
    @ApiModelProperty(value = "父菜单ID，一级菜单为0")
    @NotNull(message = "上级菜单不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer parentId;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    @NotBlank(message = "菜单名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    /**
     * 菜单URL
     */
    @ApiModelProperty(value = "菜单URL")
    private String url;
    /**
     * 授权(多个用逗号分隔，如：user:list,user:create)
     */
    @ApiModelProperty(value = "授权(多个用逗号分隔，如：user:list,user:create)")
    private String perms;
    /**
     * 类型   0：目录   1：菜单   2：按钮
     */
    @ApiModelProperty(value = "类型   0：目录   1：菜单   2：按钮")
    @NotNull(message = "类型   0：目录   1：菜单   2：按钮", groups = {AddGroup.class, UpdateGroup.class})
    private Integer type;
    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String icon;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer orders;


}