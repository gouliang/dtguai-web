package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.sys.model.SysDefConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: guoLiang
 * @date：2018-12-11 02:43:01
 * @mail 123086461@qq.com
 */
public interface SysDefConfigService extends BaseService<SysDefConfig> {
    /**
     * deleteBatchById
     *
     * @param ids ids
     */
    void deleteBatchById(List<Integer> ids);

    /**
     * getValue
     *
     * @param key key
     * @return String
     */
    String getValue(String key);

    /**
     * getConfigObject
     *
     * @param key   key
     * @param clazz clazz
     * @param <T>   T
     * @return T
     */
    <T> T getConfigObject(String key, Class<T> clazz);

    /**
     * updateValueByKey
     *
     * @param key   key
     * @param value value
     */
    @Transactional(rollbackFor = Exception.class)
    void updateValueByKey(String key, String value);
}