package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.SysDefLog;

/**
 * @author guoLiang
 * @date 2018-12-11 02:43:02
 */
public interface SysDefLogDao extends BaseMapper<SysDefLog> {


}