package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.sys.model.menu.SysDefMenu;

import java.util.List;

/**
 * @author: guoLiang
 * @date：2018-12-11 02:43:02
 * @mail 123086461@qq.com
 */
public interface SysDefMenuService extends BaseService<SysDefMenu> {

    /**
     * queryNotButtonList
     *
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> queryNotButtonList();

    /**
     * List<SysDefMenu>
     *
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> getAdminMenuList();

    /**
     * queryListParentId
     *
     * @param parentId   parentId
     * @param menuIdList menuIdList
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> queryListParentId(Integer parentId, List<Integer> menuIdList);

    /**
     * getListParentId
     *
     * @param parentId parentId
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> getListParentId(Integer parentId);

    /**
     * getAdminMenuList
     * @param menu menu对象查询参数
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> getAdminMenuList(SysDefMenu menu);

    /**
     * delete
     *
     * @param menuId menuId
     */
    void delete(Integer menuId);
}