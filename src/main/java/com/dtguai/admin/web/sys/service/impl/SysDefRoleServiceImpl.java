package com.dtguai.admin.web.sys.service.impl;

import com.dtguai.admin.base.service.impl.BaseServiceImpl;
import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.common.exception.DefinedException;
import com.dtguai.admin.page.PageModel;
import com.dtguai.admin.util.constant.Constant;
import com.dtguai.admin.util.redis.RedisUtil;
import com.dtguai.admin.web.sys.dao.SysDefRoleDao;
import com.dtguai.admin.web.sys.model.SysDefRole;
import com.dtguai.admin.web.sys.service.SysDefAdminRoleService;
import com.dtguai.admin.web.sys.service.SysDefRoleMenuService;
import com.dtguai.admin.web.sys.service.SysDefRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author guoLiang
 * @date 2018-12-11 02:43:06
 */
@Service("sysDefRoleService")
public class SysDefRoleServiceImpl extends BaseServiceImpl<SysDefRole> implements SysDefRoleService {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    private SysDefRoleMenuService sysDefRoleMenuService;

    @Autowired
    private SysDefAdminRoleService sysDefAdminRoleService;

    /**
     * 根据条件查询  不分页
     */
    @Override
    public List<SysDefRole> selectBySysDefRole(PageModel sysDefRole) {
        return selectBySysDefRolePage(sysDefRole, false, SysDefRole.class);
    }

    /**
     * 根据条件查询  分页
     */
    @Override
    public List<SysDefRole> selectBySysDefRolePage(PageModel sysDefRole) {
        return selectBySysDefRolePage(sysDefRole, true, SysDefRole.class);
    }

    @Override
    public List<SysDefRole> selectBySysDefRolePage(PageModel sysDefRole, boolean page, Class<?> clas) {
        return selectByExample(super.getExample(sysDefRole, page, clas));
    }


    /**
     * 查询用户创建的角色列表
     */
    @Override
    public List<Integer> getRoleIdList(Integer id) {
        return ((SysDefRoleDao) getMapper()).findRoleIdList(id);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(SysDefRole role) {
        insertSelective(role);

        //检查权限是否越权
        checkPrems(role, "添加");

        //保存角色与菜单关系
        sysDefRoleMenuService.saveOrUpdate(role.getId(), role.getMenuIdList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysDefRole role) {
        this.updateNotNull(role);
        //检查权限是否越权
        checkPrems(role, "更新");

        //更新角色与菜单关系
        sysDefRoleMenuService.saveOrUpdate(role.getId(), role.getMenuIdList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatchById(List<Integer> roleIds) {
        //删除角色
        Example example = new Example(SysDefRole.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", roleIds);
        (getMapper()).deleteByExample(example);

        //删除角色与菜单关联
        sysDefRoleMenuService.deleteBatchById(roleIds);

        //删除角色与用户关联
        sysDefAdminRoleService.deleteBatchById(roleIds);
    }


    /**
     * 检查权限是否越权
     */
    private void checkPrems(SysDefRole role, String operate) {

        //获取当前登录人id
        int adminId = getAdminId();

        //如果不是超级管理员，则需要判断角色的权限是否超过自己的权限
        if (adminId == Constant.SUPER_ADMIN) {
            return;
        }

        //查询用户所拥有的菜单列表 -查询用户的所有菜单ID
        List<Integer> menuIdList = sysDefAdminRoleService.getMenuByAdminId(adminId);

        //判断是否越权
        if (!menuIdList.containsAll(role.getMenuIdList())) {
            throw new DefinedException(ErrorCode.NOT_PERMISSION, operate + "角色的权限，已超出你的权限范围");
        }
    }

}