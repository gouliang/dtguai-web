package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.sys.model.SysDefLog;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:02
   */
public interface SysDefLogService extends BaseService<SysDefLog> {



}