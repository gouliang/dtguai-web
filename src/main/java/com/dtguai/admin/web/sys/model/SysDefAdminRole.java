package com.dtguai.admin.web.sys.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.ibatis.type.Alias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.page.PageModel;

/**
   * @author: guoLiang
   * @date：2018-12-11 01:57:04
   * @描述：用户与角色对应关系
   */	
@Alias("sysDefAdminRole")
@Table(name = "sys_def_admin_role")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class SysDefAdminRole extends PageModel implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	public SysDefAdminRole() {
		super();
	}
	
	public SysDefAdminRole(Integer id) {
		super();
		super.setId(id);
	}
	
	/**
	 * 用户ID
	 */							
	@ApiModelProperty(value = "用户ID")
	@Column(name = "sys_def_admin_id")
	private Integer sysDefAdminId;
	/**
	 * 角色ID
	 */							
	@ApiModelProperty(value = "角色ID")
	@Column(name = "sys_def_role_id")
	private Integer sysDefRoleId;

}