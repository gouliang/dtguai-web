package com.dtguai.admin.web.sys.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.ibatis.type.Alias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.page.PageModel;

/**
   * @author: guoLiang
   * @date：2018-12-11 01:57:04
   * @描述：系统配置信息表
   */	
@Alias("sysDefConfig")
@Table(name = "sys_def_config")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class SysDefConfig extends PageModel implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	public SysDefConfig() {
		super();
	}
	
	public SysDefConfig(Integer id) {
		super();
		super.setId(id);
	}
	
	/**
	 * key
	 */							
	@ApiModelProperty(value = "key")
	@Column(name = "param_key")
	private String paramKey;
	/**
	 * value
	 */							
	@ApiModelProperty(value = "value")
	@Column(name = "param_value")
	private String paramValue;
	/**
	 * 状态   0：隐藏   1：显示
	 */							
	@ApiModelProperty(value = "状态   0：隐藏   1：显示")
	@Column(name = "status")
	private Integer status;
	/**
	 * 备注
	 */							
	@ApiModelProperty(value = "备注")
	@Column(name = "remark")
	private String remark;

}