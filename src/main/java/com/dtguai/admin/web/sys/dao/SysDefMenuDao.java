package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.menu.SysDefMenu;

import java.util.List;

/**
 * @author guoLiang
 * @date 2019年11月4日13:49:26
 */
public interface SysDefMenuDao extends BaseMapper<SysDefMenu> {

    /**
     * 根据父菜单，查询子菜单
     *
     * @param parentId 父菜单ID
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> findListParentId(Integer parentId);

    /**
     * 获取不包含按钮的菜单列表
     *
     * @return List<SysDefMenu>
     */
    List<SysDefMenu> findNotButtonList();
}