package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.SysDefConfig;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @author guoLiang
 * @date 2019年11月4日13:47:30
 */
public interface SysDefConfigDao extends BaseMapper<SysDefConfig> {

    /**
     * 根据key，更新value
     *
     * @param paramKey   paramKey
     * @param paramValue paramValue
     */
    @Update("update sys_def_config set param_value = #{paramValue} where param_key = #{paramKey}")
    void updateValueByKey(@Param("paramKey") String paramKey, @Param("paramValue") String paramValue);

}