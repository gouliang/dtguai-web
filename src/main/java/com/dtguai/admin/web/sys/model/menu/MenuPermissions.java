package com.dtguai.admin.web.sys.model.menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @author: guoLiang
 * @date：2018年12月20日10:01:58
 * @描述：web获取左边菜单和权限
 */
@Data
@AllArgsConstructor
public class MenuPermissions {

    /**
     * 左侧菜单
     */
    @ApiModelProperty(value = "获取左侧菜单")
    private List<SysDefMenu> menuList;
    /**
     * 权限列表
     */
    @ApiModelProperty(value = "菜单名称")
    private Set<String> permissions;

}