package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.SysDefRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author guoLiang
 * @date 2019年11月4日13:49:40
 */
public interface SysDefRoleDao extends BaseMapper<SysDefRole> {

    /**
     * 查询用户创建的角色列表
     *
     * @param id id
     * @return List<Integer>
     */
    @Select("SELECT id FROM sys_def_role WHERE create_admin_id = #{id}")
    List<Integer> findRoleIdList(@Param("id") Integer id);

}