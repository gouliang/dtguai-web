package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.SysDefAdminRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
/**
   * @author guoLiang
   * @date 2018-12-11 02:43:01
   */
public interface SysDefAdminRoleDao extends BaseMapper<SysDefAdminRole> {

	/**
	 * 查询用户所有的菜单id
	 * @param id id
	 * @return List<Integer>
	 */
	@Select("SELECT DISTINCT rm.sys_def_menu_id FROM sys_def_admin_role ar LEFT JOIN sys_def_role_menu rm ON ar.sys_def_role_id = rm.sys_def_role_id WHERE ar.sys_def_admin_id = #{id}")
	List<Integer> getMenuByAdminId(@Param("id") Integer id);

	/**
	 * 获取用户所属的角色列表
	 * @param id id
	 * @return List<Integer>
	 */
	@Select("SELECT sys_def_role_id FROM sys_def_admin_role WHERE sys_def_admin_id = #{id}")
	List<Integer> getRoleIdList(@Param("id") Integer id);

	/**
	 * 根据角色ID数组，批量删除
	 * @param roleIds roleIds
	 * @return Integer
	 */
	Integer deleteBatchByRoleId(Integer[] roleIds);
	
}