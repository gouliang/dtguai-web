package com.dtguai.admin.web.sys.oauth2;

import com.alibaba.fastjson.JSON;
import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.util.HttpContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * oauth2过滤器
 *
 * @author guo
 * @date 2019年10月29日15:37:49
 */
@Slf4j
public class Oauth2Filter extends AuthenticatingFilter {

    /**
     * 3根据请求，生成一个令牌
     */
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        // 获取请求token
        String token = getRequestToken((HttpServletRequest) request);

        if (StringUtils.isBlank(token)) {
            return null;
        }

        return new Oauth2Token(token);
    }

    /**
     * 1根据请求信息，参数等信息判断是否允许通过，如果返回false，则是不通过。最终是否去访问web处理，有isAccessAllowed，
     * onAccessDenied方法共同或运算决定，只要有一个是true就会访问web控制器或action。
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        //AJAX进行跨域请求时的预检，需要向另外一个域名的资源发送一个HTTP OPTIONS请求头，用以判断实际发送的请求是否安全
        if (((HttpServletRequest) request).getMethod().equals(RequestMethod.OPTIONS.name())) {
            return true;
        }
        return false;
    }

    /**
     * 2根据请求，拒绝通过处理，如果返回false，则不再去访问web控制器或action
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        // 获取请求token，如果token不存在，直接返回401
        String token = getRequestToken((HttpServletRequest) request);
        if (StringUtils.isBlank(token)) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setContentType("application/json;charset=utf-8");
            httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
            httpResponse.setHeader("Access-Control-Allow-Origin", HttpContextUtils.getOrigin());

            httpResponse.getWriter().print(JSON.toJSONString(new ApiResponse(ErrorCode.SC_UNAUTHORIZED)));
            return false;
        }

        return executeLogin(request, response);
    }

    /**
     * 登录失败处理（认证令牌验证失败）
     */
    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request,
                                     ServletResponse response) {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setContentType("application/json;charset=utf-8");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        httpResponse.setHeader("Access-Control-Allow-Origin", HttpContextUtils.getOrigin());
        try {
            // 处理登录失败的异常
            httpResponse.getWriter().print(JSON.toJSONString(new ApiResponse(ErrorCode.SC_UNAUTHORIZED)));
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
        }

        return false;
    }

    /**
     * 获取请求的token
     */
    private String getRequestToken(HttpServletRequest httpRequest) {
        // 从header中获取token
        String token = httpRequest.getHeader("token");

        // 如果header中不存在token，则从参数中获取token
        if (StringUtils.isBlank(token)) {
            token = httpRequest.getParameter("token");
        }

        return token;
    }

}
