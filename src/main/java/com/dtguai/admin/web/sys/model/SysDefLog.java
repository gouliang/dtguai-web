package com.dtguai.admin.web.sys.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModelProperty;
import com.dtguai.admin.page.PageModel;

/**
 * @author guoLiang
 * @date 2019年7月9日11:07:49
 * @描述 系统日志
 */
@Alias("sysDefLog")
@Table(name = "sys_def_log")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Accessors(chain = true)
public class SysDefLog extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public SysDefLog() {
        super();
    }

    public SysDefLog(Integer id) {
        super();
        super.setId(id);
    }

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    @Column(name = "user_name")
    private String userName;
    /**
     * 用户操作
     */
    @ApiModelProperty(value = "用户操作")
    @Column(name = "operation")
    private String operation;
    /**
     * 请求方法
     */
    @ApiModelProperty(value = "请求方法")
    @Column(name = "method")
    private String method;
    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数")
    @Column(name = "params")
    private String params;
    /**
     * 执行时长(毫秒)
     */
    @ApiModelProperty(value = "执行时长(毫秒)")
    @Column(name = "time")
    private Long time;
    /**
     * IP地址
     */
    @ApiModelProperty(value = "IP地址")
    @Column(name = "ip")
    private String ip;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "旧信息")
    @Column(name = "old")
    private String old;

}