package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.sys.model.SysDefAdminRole;

import java.util.List;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:01
   */
public interface SysDefAdminRoleService extends BaseService<SysDefAdminRole>{

    /**
     * 查询用户的所有菜单ID
     * @param id
     * @return List<Integer>
     */
    List<Integer> getMenuByAdminId(Integer id);

    /**
     * getRoleIdList
     * @param id
     * @return List<Integer>
     */
    List<Integer> getRoleIdList(Integer id);

    /**
     * saveOrUpdate
     * @param id id
     * @param roleIdList 角色id list
     */
    void saveOrUpdate(Integer id, List<Integer> roleIdList);

    /**
     * 根据角色ID数组，批量删除
     * @param roleIds 角色ids
     * @return Integer
     */
    Integer deleteBatchById(List<Integer> roleIds);
}