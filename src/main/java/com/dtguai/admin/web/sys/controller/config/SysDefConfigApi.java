package com.dtguai.admin.web.sys.controller.config;


import com.github.pagehelper.PageInfo;
import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.common.annotation.SysLog;
import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.common.validator.BaseAssert;
import com.dtguai.admin.common.validator.ValidatorUtils;
import com.dtguai.admin.common.validator.group.AddGroup;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import com.dtguai.admin.web.sys.model.SysDefConfig;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.web.sys.form.config.ConfigForm;
import com.dtguai.admin.web.sys.service.SysDefConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author  guoLiang
 * @date 2018-12-11 02:43:01
 */
@RestController
@RequestMapping("/web/sys/controller/config")
@Api(value = "web-系统配置信息表接口", tags = {"web-系统配置信息表接口"})
public class SysDefConfigApi extends BaseAction {

    @Autowired
    private SysDefConfigService sysDefConfigService;


    /**
     * 所有配置列表
     */
    @ApiOperation(value = "所有配置列表", notes = "所有配置列表")
    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:config:list")
    public ResponseEntity<ApiResponse<PageInfo<SysDefConfig>>> list(ConfigForm sysDefConfig) {
        //状态   0：隐藏   1：显示
        sysDefConfig.setStatus(1);
        List<SysDefConfig> sysDefConfigList = sysDefConfigService.selectByPage(sysDefConfig,true,SysDefConfig.class);
        return ResponseEntity.ok(new ApiResponse<PageInfo<SysDefConfig>>(new PageInfo<SysDefConfig>(sysDefConfigList)));
    }

    /**
     * 配置信息
     */
    @ApiOperation(value = "根据主键查询配置信息", notes = "根据主键查询配置信息")
    @GetMapping(value = "/info", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:config:info")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 1022, message = "id不能为空")
    })
    @ApiImplicitParam(name = "id", value = "主键id", required = true, example = "")
    public ResponseEntity<ApiResponse<SysDefConfig>> info(Integer id) {
        BaseAssert.isNull(id, ErrorCode.ID_NOT_NULL);
        SysDefConfig sysDefConfig = sysDefConfigService.selectByKey(id);
        return ResponseEntity.ok(new ApiResponse<SysDefConfig>(sysDefConfig));
    }

    /**
     * 保存配置
     */
    @SysLog("保存配置")
    @ApiOperation(value = "添加配置", notes = "添加配置")
    @PostMapping(value = "/save", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:config:save")
    public ResponseEntity<ApiResponse<Integer>> save(@RequestBody ConfigForm config) {
        ValidatorUtils.validateEntity(config, AddGroup.class);
        SysDefConfig sysDefConfigAdd = new ModelMapper().map(config, SysDefConfig.class);
        sysDefConfigService.insertSelective(sysDefConfigAdd);
        return ResponseEntity.ok(new ApiResponse<Integer>(sysDefConfigAdd.getId()));
    }

    /**
     * 修改配置
     */
    @SysLog("修改配置")
    @ApiOperation(value = "更新", notes = "更新")
    @PostMapping(value = "/update", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:config:update")
    public ResponseEntity<ApiResponse> update(@RequestBody ConfigForm config) {
        ValidatorUtils.validateEntity(config, UpdateGroup.class);
        sysDefConfigService.updateNotNull(new ModelMapper().map(config, SysDefConfig.class));
        return ResponseEntity.ok(new ApiResponse<>());
    }


    /**
     * 删除配置
     */
    @SysLog("删除配置")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping(value = "/delete", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:config:delete")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 1022, message = "id不能为空")
    })
    @ApiImplicitParam(name = "ids", value = "主键id", required = true, example = "")
    public ResponseEntity<ApiResponse> delete(@RequestBody List<Integer> ids) {
        BaseAssert.isNull(ids, ErrorCode.ID_NOT_NULL);
        sysDefConfigService.deleteBatchById(ids);
        return ResponseEntity.ok(new ApiResponse<>());
    }


}