package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.web.sys.model.SysAdminToken;

/**
 * 用户Token
 * 
 * @author guo
 */
public interface SysUserTokenService {

	/**
	 * 生成token
	 * @param adminId  管理员ID
	 * @return SysAdminToken
	 */
	SysAdminToken createToken(Integer adminId);

	/**
	 * 退出，删除token值
	 * @param adminId  管理员ID
	 */
	void logout(Integer adminId);


	/**
	 * 查询token
	 * @param adminId  管理员ID
	 * @return SysAdminToken
	 */
	SysAdminToken getToken(Integer adminId);

	/**
	 * 查询token
	 * @param token  用户token
	 * @return SysAdminToken
	 */
	SysAdminToken getToken(String token);

	/**
	 * 刷新token
	 * @param token  用户token
	 */
	void refreshTokenTime(String token);
}
