package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.page.PageModel;
import com.dtguai.admin.web.sys.model.SysDefRole;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:06
   */
public interface SysDefRoleService extends BaseService<SysDefRole> {

    /**
     * 根据条件查询
     * @param sysDefRole
     * @return List<SysDefRole>
     */
    List<SysDefRole> selectBySysDefRole(PageModel sysDefRole);

    /**
     * 根据条件分页查询
     * @param sysDefRole sysDefRole
     * @return List<SysDefRole>
     */
    List<SysDefRole> selectBySysDefRolePage(PageModel sysDefRole);

    /**
     * 根据条件查询
     * @param sysDefRole sysDefRole
     * @param page 是否分页
     * @param clas 类型
     * @return List<SysDefRole>
     */
    List<SysDefRole> selectBySysDefRolePage(PageModel sysDefRole, boolean page, Class<?> clas);

    /**
     *getRoleIdList
     * @param   id id
     * @return  List<Integer>
     */
    List<Integer> getRoleIdList(Integer id);

    /**
     * add
     * @param role role
     */
    @Transactional(rollbackFor = Exception.class)
    void add(SysDefRole role);

    /**
     * update
     * @param role role
     */
    @Transactional(rollbackFor = Exception.class)
    void update(SysDefRole role);

    /**
     * deleteBatchById
     * @param roleIds roleIds
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteBatchById(List<Integer> roleIds);

}