package com.dtguai.admin.web.sys.service.impl;

import com.dtguai.admin.base.service.impl.BaseServiceImpl;
import com.dtguai.admin.util.redis.RedisUtil;
import com.dtguai.admin.web.sys.dao.SysDefAdminRoleDao;
import com.dtguai.admin.web.sys.model.SysDefAdminRole;
import com.dtguai.admin.web.sys.service.SysDefAdminRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
   * @author: guo
   */
@Service("sysDefAdminRoleService")
public class SysDefAdminRoleServiceImpl extends BaseServiceImpl<SysDefAdminRole> implements SysDefAdminRoleService  {
	
	@Autowired
	RedisUtil redisUtil;


	/**
	 * 查询用户所有的菜单id
	 */
	@Override
	public List<Integer> getMenuByAdminId(Integer id) {
		return ((SysDefAdminRoleDao) getMapper()).getMenuByAdminId(id);
	}

	/**
	 * 获取用户所属的角色列表
	 */
	@Override
	public List<Integer> getRoleIdList(Integer id) {
		return ((SysDefAdminRoleDao) getMapper()).getRoleIdList(id);
	}


	@Override
	public void saveOrUpdate(Integer id, List<Integer> roleIdList) {
		//先删除用户与角色关系
		SysDefAdminRole del = new SysDefAdminRole();
		del.setSysDefAdminId(id);
		deleteByWhere(del);

		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}

		//保存用户与角色关系
		List<SysDefAdminRole> list;
		list = new ArrayList<>(roleIdList.size());

		roleIdList.forEach(x ->{
			SysDefAdminRole adminRole = new SysDefAdminRole();
			adminRole.setSysDefAdminId(id);
			adminRole.setSysDefRoleId(x);
			list.add(adminRole);
		});
		//批量插入
		((SysDefAdminRoleDao) getMapper()).insertList(list);
	}

	/**
	 * 根据角色ID数组，批量删除
	 */
	@Override
	public Integer deleteBatchById(List<Integer> roleIds) {
		Example example = new Example(SysDefAdminRole.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andIn("sysDefRoleId",roleIds);
		return ((SysDefAdminRoleDao) getMapper()).deleteByExample(example);
	}

}