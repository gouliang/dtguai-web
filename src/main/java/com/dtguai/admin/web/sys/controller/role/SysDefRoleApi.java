package com.dtguai.admin.web.sys.controller.role;


import com.dtguai.admin.base.action.BaseAction;
import com.dtguai.admin.common.annotation.SysLog;
import com.dtguai.admin.common.error.ErrorCode;
import com.dtguai.admin.common.validator.BaseAssert;
import com.dtguai.admin.common.validator.ValidatorUtils;
import com.dtguai.admin.common.validator.group.AddGroup;
import com.dtguai.admin.common.validator.group.UpdateGroup;
import com.dtguai.admin.protocol.response.ApiResponse;
import com.dtguai.admin.util.constant.Constant;
import com.dtguai.admin.web.sys.form.role.RoleForm;
import com.dtguai.admin.web.sys.model.SysDefRole;
import com.dtguai.admin.web.sys.service.SysDefRoleMenuService;
import com.dtguai.admin.web.sys.service.SysDefRoleService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: guoLiang
 * @date 2018-12-11 02:43:06
 */
@RestController
@RequestMapping("/web/sys/controller/role")
@Api(value = "web-角色接口", tags = {"web-角色接口"})
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SysDefRoleApi extends BaseAction {

    private SysDefRoleMenuService sysDefRoleMenuService;

    private SysDefRoleService sysDefRoleService;

    /**
     * 角色列表
     */
    @ApiOperation(value = "分页查询角色列表-返回PageInfo", notes = "分页查询角色列表-返回PageInfo")
    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:role:list")
    public ResponseEntity<ApiResponse<PageInfo<SysDefRole>>> list(RoleForm role) {
        //如果不是超级管理员，则只查询自己创建的角色列表
        if (getAdminId() != Constant.SUPER_ADMIN) {
            role.setCreateAdminId(getAdminId());
        }
        List<SysDefRole> sysDefRoleList = sysDefRoleService.selectByPage(role, true, SysDefRole.class);
        return ResponseEntity.ok(new ApiResponse<PageInfo<SysDefRole>>(new PageInfo<SysDefRole>(sysDefRoleList)));
    }

    /**
     * 角色列表
     */
    @ApiOperation(value = "不分页查询角色列表", notes = "不分页查询角色列表")
    @GetMapping(value = "/select", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:role:select")
    public ResponseEntity<ApiResponse<PageInfo<SysDefRole>>> select() {
        SysDefRole role = new SysDefRole();
        //如果不是超级管理员，则只查询自己所拥有的角色列表
        if (getAdminId() != Constant.SUPER_ADMIN) {
            role.setCreateAdminId(getAdminId());
        }
        List<SysDefRole> sysDefRoleList = sysDefRoleService.selectBySysDefRole(role);
        return ResponseEntity.ok(new ApiResponse<>(new PageInfo<>(sysDefRoleList)));
    }

    /**
     * 角色信息
     */
    @ApiOperation(value = "根据id查询角色信息", notes = "根据id查询角色信息")
    @GetMapping(value = "/info", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:role:info")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 1022, message = "id不能为空")
    })
    @ApiImplicitParam(name = "id", value = "主键id", required = true, example = "")
    public ResponseEntity<ApiResponse<SysDefRole>> info(Integer id) {
        BaseAssert.isNull(id, ErrorCode.ID_NOT_NULL);
        SysDefRole role = sysDefRoleService.selectByKey(id);
        //查询角色对应的菜单
        List<Integer> menuIdList = sysDefRoleMenuService.getMenuIdList(id);
        role.setMenuIdList(menuIdList);
        return ResponseEntity.ok(new ApiResponse<>(role));
    }


    /**
     * 保存角色
     */
    @SysLog("保存角色")
    @ApiOperation(value = "添加", notes = "添加")
    @PostMapping(value = "/save", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:role:save")
    public ResponseEntity<ApiResponse<Integer>> save(@RequestBody RoleForm role) {
        ValidatorUtils.validateEntity(role, AddGroup.class);
        SysDefRole sysDefRoleAdd = new ModelMapper().map(role, SysDefRole.class).setCreateAdminId(getAdminId());
        sysDefRoleService.add(sysDefRoleAdd);
        return ResponseEntity.ok(new ApiResponse<>(sysDefRoleAdd.getId()));
    }

    /**
     * 修改角色
     */
    @SysLog("修改角色")
    @ApiOperation(value = "更新", notes = "更新")
    @PostMapping(value = "/update", produces = "application/json;charset=UTF-8")
    @RequiresPermissions("sys:role:update")
    public ResponseEntity<ApiResponse> update(@RequestBody RoleForm role) {
        ValidatorUtils.validateEntity(role, UpdateGroup.class);
        sysDefRoleService.update(new ModelMapper().map(role, SysDefRole.class).setUpdateAdminId(getAdminId()));
        return ResponseEntity.ok(new ApiResponse());
    }

    /**
     * 删除角色
     */
    @SysLog("删除角色")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping(value = "/delete", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @io.swagger.annotations.ApiResponse(code = 1022, message = "id不能为空")
    })
    @RequiresPermissions("sys:role:delete")
    @ApiImplicitParam(name = "roleIds", value = "主键id", required = true, example = "")
    public ResponseEntity<ApiResponse> delete(@RequestBody List<Integer> roleIds) {
        BaseAssert.isNull(roleIds, ErrorCode.ID_NOT_NULL);
        sysDefRoleService.deleteBatchById(roleIds);
        return ResponseEntity.ok(new ApiResponse());
    }


}