package com.dtguai.admin.web.sys.dao;

import com.dtguai.admin.base.mapper.BaseMapper;
import com.dtguai.admin.web.sys.model.SysDefAdmin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author guoLiang
 * @date 2018-12-11 02:43:01
 */
public interface SysDefAdminDao extends BaseMapper<SysDefAdmin> {

    /**
     * Select标签 查询
     *
     * @param id id
     * @return List<String>
     */
    @Select("SELECT DISTINCT m.perms FROM sys_def_admin_role ar LEFT JOIN sys_def_role_menu rm  " +
            "ON ar.sys_def_role_id = rm.sys_def_role_id LEFT JOIN sys_def_menu m ON rm.sys_def_menu_id = m.id " +
            "WHERE ar.sys_def_admin_id =#{id}")
    List<String> findPermsById(@Param("id") Integer id);

    /**
     * 修改密码
     *
     * @param id          id
     * @param password    旧密码
     * @param newPassword 新密码
     * @return Integer
     */
    @Update("UPDATE sys_def_admin SET PASSWORD =#{newPassword} WHERE PASSWORD =#{password} AND id =#{id}")
    Integer updatePassword(@Param("id") Integer id, @Param("password") String password, @Param("newPassword") String newPassword);
}