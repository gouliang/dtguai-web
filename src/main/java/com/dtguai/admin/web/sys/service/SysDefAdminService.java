package com.dtguai.admin.web.sys.service;

import com.dtguai.admin.base.service.BaseService;
import com.dtguai.admin.web.sys.model.SysDefAdmin;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
   * @author guoLiang
   * @date 2018-12-11 02:43:01
   */
public interface SysDefAdminService extends BaseService<SysDefAdmin> {


    /**
     * 获取用户的权限列表
     * @param adminId
     * @return List<String>
     */
    List<String> getAdminPermsList(Integer adminId);

    /**
     * 修改密码
     * @param adminId 管理员id
     * @param password 旧密码
     * @param newPassword 新密码
     * @return boolean
     */
    boolean updatePassword(Integer adminId, String password, String newPassword);

    /**
     * add
     * @param sysDefAdmin sysDefAdmin
     * @return Integer
     */
    @Transactional(rollbackFor = Exception.class)
    Integer add(SysDefAdmin sysDefAdmin);

    /**
     * update
     * @param admin admin
     */
    @Transactional(rollbackFor = Exception.class)
    void update(SysDefAdmin admin);

    /**
     * 删除用户
     * @param ids id的list
     */
    void deleteBatchById(List<Integer> ids);
}