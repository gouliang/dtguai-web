package com.dtguai.admin.web.sys.service.impl;

import com.dtguai.admin.base.service.impl.BaseServiceImpl;
import com.dtguai.admin.util.constant.Constant;
import com.dtguai.admin.util.redis.RedisUtil;
import com.dtguai.admin.web.sys.dao.SysDefMenuDao;
import com.dtguai.admin.web.sys.model.SysDefRoleMenu;
import com.dtguai.admin.web.sys.model.menu.SysDefMenu;
import com.dtguai.admin.web.sys.service.SysDefAdminRoleService;
import com.dtguai.admin.web.sys.service.SysDefMenuService;
import com.dtguai.admin.web.sys.service.SysDefRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guoLiang
 * @date 2018-12-11 02:43:02
 */
@Service("sysDefMenuService")
public class SysDefMenuServiceImpl extends BaseServiceImpl<SysDefMenu> implements SysDefMenuService {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    SysDefAdminRoleService sysDefAdminRoleService;

    @Autowired
    private SysDefRoleMenuService sysDefRoleMenuService;

    public static final Integer ROOT_MENU_ID = 0;

    @Override
    public List<SysDefMenu> queryListParentId(Integer parentId, List<Integer> menuIdList) {
        List<SysDefMenu> menuList = getListParentId(parentId);
        if (menuIdList == null) {
            return menuList;
        }

        List<SysDefMenu> userMenuList = new ArrayList<>();
        for (SysDefMenu menu : menuList) {
            if (menuIdList.contains(menu.getId())) {
                userMenuList.add(menu);
            }
        }
        return userMenuList;
    }

    @Override
    public List<SysDefMenu> getListParentId(Integer parentId) {
        return ((SysDefMenuDao) getMapper()).findListParentId(parentId);
    }

    @Override
    public List<SysDefMenu> queryNotButtonList() {
        return ((SysDefMenuDao) getMapper()).findNotButtonList();
    }

    @Override
    public List<SysDefMenu> getAdminMenuList() {
        //系统管理员，拥有最高权限
        if (getAdminId() == Constant.SUPER_ADMIN) {
            return getAllMenuList(null);
        }
        //用户菜单列表
        List<Integer> menuIdList = sysDefAdminRoleService.getMenuByAdminId(getAdminId());
        return getAllMenuList(menuIdList);
    }

    @Override
    public List<SysDefMenu> getAdminMenuList(SysDefMenu menu) {
        menu.setOrderBy("orders asc");
        //系统管理员，拥有全部菜单权限
        if (getAdminId() == Constant.SUPER_ADMIN) {
            return selectByPage(menu, false, SysDefMenu.class);
        }

        Example example = new Example(SysDefMenu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", sysDefAdminRoleService.getMenuByAdminId(getAdminId()));
        criteria.andEqualTo(menu);
        getExample(criteria, menu, false);
        return selectByExample(example);
    }

    @Override
    public void delete(Integer menuId) {
        //删除菜单
        super.delete(menuId);
        //删除菜单与角色关联
        SysDefRoleMenu rm = new SysDefRoleMenu();
        rm.setSysDefMenuId(menuId);
        sysDefRoleMenuService.deleteByWhere(rm);
    }

    /**
     * 获取菜单列表
     */
    private List<SysDefMenu> getAllMenuList(List<Integer> menuIdList) {
        //查询根菜单列表
        List<SysDefMenu> menuList = queryListParentId(ROOT_MENU_ID, menuIdList);
        //递归获取子菜单
        getMenuTreeList(menuList, menuIdList);

        return menuList;
    }

    /**
     * 递归
     */
    private List<SysDefMenu> getMenuTreeList(List<SysDefMenu> menuList, List<Integer> menuIdList) {
        List<SysDefMenu> subMenuList = new ArrayList<SysDefMenu>();

        for (SysDefMenu entity : menuList) {
            //目录
            if (entity.getType() == Constant.MenuType.CATALOG.getValue()) {
                entity.setList(getMenuTreeList(queryListParentId(entity.getId(), menuIdList), menuIdList));
            }
            subMenuList.add(entity);
        }

        return subMenuList;
    }
}