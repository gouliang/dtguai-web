package com.dtguai;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import tk.mybatis.spring.annotation.MapperScan;

import java.util.Date;


/**
 * @author guo
 * @date 2018-09-29 03:46:03
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@Controller
@EnableTransactionManagement
@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan(basePackages = {"com.dtguai.admin.web.*.dao"})
@Slf4j
public class WebStart implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(WebStart.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("-------------------服务启动完成!{}--------------------", new Date());
        log.info("-------------------swagger-bootstrap-ui需要谷歌或者火狐浏览器--------------------");
    }

    @GetMapping(value = "/")
    public String home() {
        return "redirect:/doc.html";
    }

}
