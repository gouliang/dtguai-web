package com.dtguai.admin.sign;

import com.alibaba.fastjson.JSON;

import com.dtguai.admin.common.sign.SignAspect;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author guo
 * @date 2019年7月19日13:10:04
 */
@Slf4j
public class SignTest {


    public static final String rsa = "{" +
            "  \"name\": \"admin\"," +
            "  \"password\": \"admin\"," +
           // "  \"timestamp\": 1563514291184," +
            "  \"timestamp\": " + System.currentTimeMillis() + "," +
            "  \"captcha\": \"2a3xn\"," +
            "  \"uuid\": \"7617e8c9-8828-47d0-80e9-9473a23c0bb6\"" +
            "}";

    public static final String signKey = "qyxVsFzp";

    @Test
    public void aesEncrypt() {
        log.warn("json数据为:{}", rsa);
        Map my = JSON.parseObject(rsa, TreeMap.class);
        String newDataJson = JSON.toJSONString(my);
        log.warn("添加了时间戳,并且排好序(按key的顺序排)的json:{}", newDataJson);

        StringBuilder paramBuilder = new StringBuilder();
        my.forEach((k, v) -> {
            if (v != null && !k.equals(SignAspect.SIGN_HEADER) && !k.equals(SignAspect.TOKEN_HEADER)) {
                paramBuilder.append(k).append("=").append(v).append("&");
            }
        });
        log.warn("循环map拼接数据串把token和sign剔除:{}", paramBuilder);
        String signData = paramBuilder.append("signKey=").append(signKey).toString();
        log.warn("最后加入signkey:                  {}", signData);
        String md5Sign = DigestUtils.md5Hex(signData);
        log.warn("signStr-md5加密:{}", md5Sign);

    }

}
