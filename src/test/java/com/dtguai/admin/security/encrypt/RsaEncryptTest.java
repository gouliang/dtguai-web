package com.dtguai.admin.security.encrypt;

import com.dtguai.admin.security.decrypt.RsaDecryptTest;
import com.dtguai.admin.util.security.RsaEncryptUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import java.util.Map;

/**
 * 加密
 *
 * @author guo
 * @date 2019年6月25日15:59:40
 */
@Slf4j
public class RsaEncryptTest {

    public static final String dataJson = "{" +
            "  \"name\": \"admin\"," +
            "  \"password\": \"admin\"," +
            "  \"timestamp\": 1563520546434," +
            //"  \"timestamp\": " + System.currentTimeMillis() + "," +
            "  \"captcha\": \"2a3xn\"," +
            "  \"uuid\": \"7617e8c9-8828-47d0-80e9-9473a23c0bb6\"" +
            "}";

    /**
     * RSA公钥
     */
    public static final String RSA_PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAou6CNXAJ7+WDxz27tjZA6oBpRFjVTOp2uqS9MWaEtVL9CplkxzVuYGhZpzsYqo1PcMa8AkjzAqRg6VM/6Q2P+OpkVDvmnHX9vRdBEWSHSwJvh/CZOmft0MAfWC5v2EJuekUqiS/1cOHo1c8djh4ekyZgngJdoJykOVBYMpwSTfPESJraHQLdg06xVgme1mHCI8wHpmEzXKGUJYjOybeAkoeWnFHq9aKRtpt51UWhOBhDumUnMh53c4y8Cykmy6zLwl0hG6ah/TSxnWOsC+4ElVlOn4VGEmUEtgowzFzjEL8vjUGeKhHBnyyejY5jz37zt1X/Gk4vtA53ZtbwdptISwIDAQAB";

    /**
     * 初始化公私钥
     */
    @Test
    public void rsaKeyInit() {
        Map<String, Object> keyMap = RsaEncryptUtil.initKey();
        //公钥
        byte[] publicKey = RsaEncryptUtil.getPublicKey(keyMap);
        //私钥
        byte[] privateKey = RsaEncryptUtil.getPrivateKey(keyMap);

        log.warn("公钥：{}" + Base64.encodeBase64String(publicKey));
        log.warn("私钥：{}" + Base64.encodeBase64String(privateKey));
    }

    /**
     * 公钥加密
     */
    @Test
    public void rsaPubEncrypt() {
        log.warn("原始的json数据为:{}", dataJson);
        log.warn("原始的json数据长度为:{}", dataJson.length());
        //公钥加密
        byte[] jiami = RsaEncryptUtil.encryptByPublicKey(dataJson.getBytes(), Base64.decodeBase64(RSA_PUB_KEY));
        log.warn("公钥加密数据:{}", Base64.encodeBase64String(jiami));
        log.warn("公钥加密数据长度:{}", jiami.length);
    }

    /**
     * 私钥加密
     */
    @Test
    public void rsaPirEncrypt() {
        log.warn("原始的json数据为:{}", dataJson);
        log.warn("原始的json数据长度为:{}", dataJson.length());
        //私钥加密
        String jiami = RsaEncryptUtil.encrypt(RsaEncryptTest.dataJson, RsaDecryptTest.RSA_PIR_KEY);
        log.warn("私钥加密数据:{}", jiami);
        log.warn("私钥加密数据长度:{}", jiami.length());
    }

}