package com.dtguai.admin.security.encrypt;

import com.dtguai.admin.util.security.AesEncryptUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 加密
 *
 * @author guo
 * @date 2019年6月25日17:26:17
 */
@Slf4j
public class AesEncryptTest {

    public static final String dataJson = "{" +
            "  \"createTime\": \"2019-04-10 10:34:57\"," +
            "  \"id\": 0," +
            "  \"imei\": 11111," +
            "  \"mobile\": 18600231871," +
            "  \"name\": \"陈雄\"," +
            "  \"password\": 123456," +
            "  \"type\": 0" +
            "}";

    public static final String aesKey = "xiaoFuLoveXiaoQiu";

    @Test
    public void aesEncrypt() {
        log.warn("加密前的json数据为:{}", dataJson);
        String jiami = AesEncryptUtil.encrypt(dataJson, aesKey);
        log.warn("加密后的数据为:{}", jiami);
    }

}
