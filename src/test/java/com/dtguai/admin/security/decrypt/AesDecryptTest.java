package com.dtguai.admin.security.decrypt;

import com.alibaba.fastjson.JSON;

import com.dtguai.admin.common.exception.DefinedException;
import com.dtguai.admin.util.security.AesEncryptUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;

/**
 * 解密
 *
 * @Auther: guo
 * @Date: 2019/4/17 10:25
 * @Description:
 */
@Slf4j
public class AesDecryptTest {

    public static final String dataJson = "{" +
            "  \"createTime\": \"2019-04-10 10:34:57\"," +
            "  \"id\": 0," +
            "  \"imei\": 11111," +
            "  \"mobile\": 18600231871," +
            "  \"name\": \"陈雄\"," +
            "  \"password\": 123456," +
            "  \"type\": 0," +
            "  \"timestamp\": 1234" +
            "}";

    public static final String aesKey = "xiaoFuLoveXiaoQiu";

    public static final String rep = "{\"code\":200,\"msg\":\"成功\",\"result\":[]}";

    @Test
    public void aesEncrypt() {

        log.warn("加密前的json数据为:{}", dataJson);
        String jiami = AesEncryptUtil.encrypt(dataJson, aesKey);
        log.warn("加密后的数据为:{}", jiami);


        Map m = JSON.parseObject(rep, Map.class);
        m.put("result", jiami);
        log.warn("正常服务器返回的数据为:{}", JSON.toJSONString(m));

        log.warn("获取返回的result数据为:{}", m.get("result"));

        //解密
        String jiemi = AesEncryptUtil.decrypt(jiami, aesKey);
        log.warn("解密后的json数据为:{}", jiemi);


        Optional.ofNullable(dataJson)
                .map(x -> JSON.parseObject(x, Map.class))
                .map(x -> x.get("timestamp"))
                //.map(x -> (long) x)
                .orElseThrow(() -> new DefinedException("数据加密timestamp不能为空"));


    }

}
